#include "../../include/ULK_json.h"

int main(int argc, char **argv)
{
   //Creat json and save to disk
   FILE *f = fopen("out.json","w");
   ULK_json5_root *r = ULK_json_create_root();
   ULK_json_object_add_string(&r->root,"test1","Hello");
   ULK_json_object_add_real(&r->root,"test2",10.0f);
   ULK_json_object_add_integer(&r->root,"test3",1342);
   ULK_json_object_add_boolean(&r->root,"test4",0);

   ULK_json5 o = ULK_json_create_object();
   ULK_json_object_add_string(&o,"test1","Hello");
   ULK_json_object_add_real(&o,"test2",10.0f);
   ULK_json_object_add_integer(&o,"test3",1342);
   ULK_json_object_add_boolean(&o,"test4",0);
   ULK_json_object_add_object(&r->root,"test5",o);

   ULK_json5 a = ULK_json_create_array();
   ULK_json_array_add_string(&a,"test");
   ULK_json_array_add_real(&a,10.0f);
   ULK_json_array_add_integer(&a,1342);
   ULK_json_array_add_boolean(&a,0);
   ULK_json_array_add_object(&a,o);
   ULK_json_object_add_array(&r->root,"test6",a);

   ULK_json_write_file(f,&r->root);
   ULK_json_free(r);
   fclose(f);
   //----------------------------------------

   //Load json again and output
   r = ULK_json_parse_file("out.json");
   ULK_json_write_file(stdout,&r->root);
   puts("");
   ULK_json_free(r);
   //----------------------------------------

   return 0;
}
