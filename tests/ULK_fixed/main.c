#include <stdio.h>
#include <math.h>
#include "../../include/ULK_fixed.h"

int main(int argc, char **argv)
{
   puts("16bit number test:--------------------------");
   ULK_fixed_16 n = ULK_fixed_16_from_int(12)+ULK_fixed_16_from_int(13);
   printf("13 + 12 = 25; ULK_fixed: %d\n",ULK_fixed_16_to_int(n));
   printf("2.5 * 4 = 10; ULK_fixed: %d\n",ULK_fixed_16_to_int(ULK_fixed_16_mul(ULK_fixed_16_div(ULK_fixed_16_from_int(5),ULK_fixed_16_from_int(2)),ULK_fixed_16_from_int(4))));
   printf("2.5 * -4 = -10; ULK_fixed: %d\n",ULK_fixed_16_to_int(ULK_fixed_16_mul(ULK_fixed_16_div(ULK_fixed_16_from_int(5),ULK_fixed_16_from_int(2)),ULK_fixed_16_from_int(-4))));
   printf("126 / 2 = 63; ULK_fixed: %d\n",ULK_fixed_16_to_int(ULK_fixed_16_div(ULK_fixed_16_from_int(126),ULK_fixed_16_from_int(2))));
   printf("round(10 / 3) = 3; ULK_fixed: %d\n",ULK_fixed_16_to_int(ULK_fixed_16_round(ULK_fixed_16_div(ULK_fixed_16_from_int(10),ULK_fixed_16_from_int(3)))));
   printf("ceil(10 / 3) = 3; ULK_fixed: %d\n",ULK_fixed_16_to_int(ULK_fixed_16_ceil(ULK_fixed_16_div(ULK_fixed_16_from_int(10),ULK_fixed_16_from_int(3)))));
   printf("round(-3 / 2) = -2; ULK_fixed: %d\n",ULK_fixed_16_to_int(ULK_fixed_16_round(ULK_fixed_16_div(ULK_fixed_16_from_int(-3),ULK_fixed_16_from_int(2)))));
   printf("floor(-3 / 2) = -1; ULK_fixed: %d\n",ULK_fixed_16_to_int(ULK_fixed_16_floor(ULK_fixed_16_div(ULK_fixed_16_from_int(-3),ULK_fixed_16_from_int(2)))));
   printf("sin( 90 ) = 1; ULK_fixed: %d\n",ULK_fixed_16_sin(ULK_fixed_16_rad_to_angle(ULK_FIXED16_PI/2)));
   printf("cos( 0 ) = 1; ULK_fixed: %d\n",ULK_fixed_16_cos(ULK_fixed_16_from_int(0)));
   printf("sqrt( 9 ) = 3; ULK_fixed: %d\n",ULK_fixed_16_to_int(ULK_fixed_16_sqrt(ULK_fixed_16_from_int(9))));
   printf("sqrt( 5 ) = 2.236068; ULK_fixed: %f\n",ULK_fixed_16_sqrt(ULK_fixed_16_from_int(5))/256.0f);
   puts("16bit number test end-----------------------");

   puts("32bit number test:--------------------------");
   ULK_fixed_32 n32 = ULK_fixed_32_from_int(12)+ULK_fixed_32_from_int(13);
   printf("13 + 12 = 25; ULK_fixed: %d\n",ULK_fixed_32_to_int(n32));
   printf("2.5 * 4 = 10; ULK_fixed: %d\n",ULK_fixed_32_to_int(ULK_fixed_32_mul(ULK_fixed_32_div(ULK_fixed_32_from_int(5),ULK_fixed_32_from_int(2)),ULK_fixed_32_from_int(4))));
   printf("2.5 * -4 = -10; ULK_fixed: %d\n",ULK_fixed_32_to_int(ULK_fixed_32_mul(ULK_fixed_32_div(ULK_fixed_32_from_int(5),ULK_fixed_32_from_int(2)),ULK_fixed_32_from_int(-4))));
   printf("126 / 2 = 63; ULK_fixed: %d\n",ULK_fixed_32_to_int(ULK_fixed_32_div(ULK_fixed_32_from_int(126),ULK_fixed_32_from_int(2))));
   printf("round(10 / 3) = 3; ULK_fixed: %d\n",ULK_fixed_32_to_int(ULK_fixed_32_round(ULK_fixed_32_div(ULK_fixed_32_from_int(10),ULK_fixed_32_from_int(3)))));
   printf("ceil(10 / 3) = 3; ULK_fixed: %d\n",ULK_fixed_32_to_int(ULK_fixed_32_ceil(ULK_fixed_32_div(ULK_fixed_32_from_int(10),ULK_fixed_32_from_int(3)))));
   printf("round(-3 / 2) = -2; ULK_fixed: %d\n",ULK_fixed_32_to_int(ULK_fixed_32_round(ULK_fixed_32_div(ULK_fixed_32_from_int(-3),ULK_fixed_32_from_int(2)))));
   printf("floor(-3 / 2) = -1; ULK_fixed: %d\n",ULK_fixed_32_to_int(ULK_fixed_32_floor(ULK_fixed_32_div(ULK_fixed_32_from_int(-3),ULK_fixed_32_from_int(2)))));
   printf("sin( 90 ) = 1; ULK_fixed: %d\n",ULK_fixed_32_sin(ULK_fixed_32_deg_to_angle(ULK_fixed_32_from_int(90))));
   printf("cos( 0 ) = 1; ULK_fixed: %d\n",ULK_fixed_32_cos(ULK_fixed_32_from_int(0)));
   printf("sqrt( 9 ) = 3; ULK_fixed: %d\n",ULK_fixed_32_to_int(ULK_fixed_32_sqrt(ULK_fixed_32_from_int(9))));
   printf("sqrt( 5 ) = 2.236068; ULK_fixed: %f\n",ULK_fixed_32_sqrt(ULK_fixed_32_from_int(5))/65536.0f);
   puts("32bit number test end-----------------------");

   puts("64bit number test:--------------------------");
   ULK_fixed_64 n64 = ULK_fixed_64_from_int(12)+ULK_fixed_64_from_int(13);
   printf("13 + 12 = 25; ULK_fixed: %d\n",ULK_fixed_64_to_int(n64));
   printf("2.5 * 4 = 10; ULK_fixed: %d\n",ULK_fixed_64_to_int(ULK_fixed_64_mul(ULK_fixed_64_div(ULK_fixed_64_from_int(5),ULK_fixed_64_from_int(2)),ULK_fixed_64_from_int(4))));
   printf("2.5 * -4 = -10; ULK_fixed: %d\n",ULK_fixed_64_to_int(ULK_fixed_64_mul(ULK_fixed_64_div(ULK_fixed_64_from_int(5),ULK_fixed_64_from_int(2)),ULK_fixed_64_from_int(-4))));
   printf("126 / 2 = 63; ULK_fixed: %d\n",ULK_fixed_64_to_int(ULK_fixed_64_div(ULK_fixed_64_from_int(126),ULK_fixed_64_from_int(2))));
   printf("round(10 / 3) = 3; ULK_fixed: %d\n",ULK_fixed_64_to_int(ULK_fixed_64_round(ULK_fixed_64_div(ULK_fixed_64_from_int(10),ULK_fixed_64_from_int(3)))));
   printf("ceil(10 / 3) = 3; ULK_fixed: %d\n",ULK_fixed_64_to_int(ULK_fixed_64_ceil(ULK_fixed_64_div(ULK_fixed_64_from_int(10),ULK_fixed_64_from_int(3)))));
   printf("round(-3 / 2) = -2; ULK_fixed: %d\n",ULK_fixed_64_to_int(ULK_fixed_64_round(ULK_fixed_64_div(ULK_fixed_64_from_int(-3),ULK_fixed_64_from_int(2)))));
   printf("floor(-3 / 2) = -1; ULK_fixed: %d\n",ULK_fixed_64_to_int(ULK_fixed_64_floor(ULK_fixed_64_div(ULK_fixed_64_from_int(-3),ULK_fixed_64_from_int(2)))));
   printf("sin( 90 ) = 1; ULK_fixed: %ld\n",ULK_fixed_64_sin(ULK_fixed_64_from_int(2)));
   printf("cos( 0 ) = 1; ULK_fixed: %ld\n",ULK_fixed_64_cos(ULK_fixed_64_from_int(0)));
   printf("sqrt( 9 ) = 3; ULK_fixed: %d\n",ULK_fixed_64_to_int(ULK_fixed_64_sqrt(ULK_fixed_64_from_int(9))));
   printf("sqrt( 5 ) = 2.236068; ULK_fixed: %f\n",ULK_fixed_64_sqrt(ULK_fixed_64_from_int(5))/4294967296.0f);
   puts("64bit number test end-----------------------");
   printf("%f\n",1.91059300966915117e-31f*1000.0f);

   return 0;
}
