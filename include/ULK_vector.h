/*

██    ██ ██      ██   ██        ██    ██ ███████  ██████ ████████  ██████  ██████  
██    ██ ██      ██  ██         ██    ██ ██      ██         ██    ██    ██ ██   ██ 
██    ██ ██      █████          ██    ██ █████   ██         ██    ██    ██ ██████  
██    ██ ██      ██  ██          ██  ██  ██      ██         ██    ██    ██ ██   ██ 
 ██████  ███████ ██   ██ ███████  ████   ███████  ██████    ██     ██████  ██   ██ 
                                                                                   
*/

/* 
LICENSE OPTION A: 3-clause BSD

   Copyright (C) 2020-2021 Captain4LK (Lukas Holzbeierlein)

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

LICENSE OPTION B: Public Domain - Unlicense

   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <http://unlicense.org/>
*/

/*

██    ██ ███████  █████   ██████  ███████ 
██    ██ ██      ██   ██ ██       ██      
██    ██ ███████ ███████ ██   ███ █████   
██    ██      ██ ██   ██ ██    ██ ██      
 ██████  ███████ ██   ██  ██████  ███████ 
                                          
-----------------------------------------

1. Add ULK_vector.c to your source files.
2. Add ULK_vector.h to your include files.
3. Edit ULK_vector.c, l.66 to match the path to ULK_vector.h

Linker options: None, unless your plattform treats math.h as a library, then add -lm (eg. Linux).
*/

#ifndef _ULK_VECTOR_H_

#define _ULK_VECTOR_H_

typedef float ULK_vector_2d[2];
typedef float ULK_vector_3d[3];
typedef float ULK_vector_4d[4];

void  ULK_vector_2d_add(ULK_vector_2d out, const ULK_vector_2d a, const ULK_vector_2d b);
float ULK_vector_2d_angle(const ULK_vector_2d a, const ULK_vector_2d b);
void  ULK_vector_2d_copy(ULK_vector_2d dst, const ULK_vector_2d src);
float ULK_vector_2d_dist(const ULK_vector_2d a, const ULK_vector_2d b);
float ULK_vector_2d_dist2(const ULK_vector_2d a, const ULK_vector_2d b);
void  ULK_vector_2d_div(ULK_vector_2d out, const ULK_vector_2d in, float s);
float ULK_vector_2d_dot(const ULK_vector_2d a, const ULK_vector_2d b);
void  ULK_vector_2d_lerp(ULK_vector_2d out, const ULK_vector_2d a, const ULK_vector_2d b, float t);
float ULK_vector_2d_mag(const ULK_vector_2d in);
float ULK_vector_2d_mag2(const ULK_vector_2d in);
void  ULK_vector_2d_mul(ULK_vector_2d out, const ULK_vector_2d in, float s);
void  ULK_vector_2d_negate(ULK_vector_2d out, const ULK_vector_2d in);
void  ULK_vector_2d_norm(ULK_vector_2d out, const ULK_vector_2d in);
void  ULK_vector_2d_rot(ULK_vector_2d out, const ULK_vector_2d in, const ULK_vector_2d origin, float radians);
void  ULK_vector_2d_set(ULK_vector_2d out, float x, float y);
void  ULK_vector_2d_sub(ULK_vector_2d out, const ULK_vector_2d a, const ULK_vector_2d b);
void  ULK_vector_2d_zero(ULK_vector_2d out);

void  ULK_vector_3d_add(ULK_vector_3d out, const ULK_vector_3d a, const ULK_vector_3d b);
float ULK_vector_3d_angle(const ULK_vector_3d a, const ULK_vector_3d b);
void  ULK_vector_3d_copy(ULK_vector_3d dst, const ULK_vector_3d src);
void  ULK_vector_3d_cross(ULK_vector_3d out, const ULK_vector_3d a, const ULK_vector_3d b);
float ULK_vector_3d_dist(const ULK_vector_3d a, const ULK_vector_3d b);
float ULK_vector_3d_dist2(const ULK_vector_3d a, const ULK_vector_3d b);
void  ULK_vector_3d_div(ULK_vector_3d out, const ULK_vector_3d in, float s);
float ULK_vector_3d_dot(const ULK_vector_3d a, const ULK_vector_3d b);
void  ULK_vector_3d_lerp(ULK_vector_3d out, const ULK_vector_3d a, const ULK_vector_3d b, float t);
float ULK_vector_3d_mag(const ULK_vector_3d in);
float ULK_vector_3d_mag2(const ULK_vector_3d in);
void  ULK_vector_3d_mul(ULK_vector_3d out, const ULK_vector_3d in, float s);
void  ULK_vector_3d_negate(ULK_vector_3d out, const ULK_vector_3d in);
void  ULK_vector_3d_norm(ULK_vector_3d out, const ULK_vector_3d in);
void  ULK_vector_3d_set(ULK_vector_3d out, float x, float y, float z);
void  ULK_vector_3d_sub(ULK_vector_3d out, const ULK_vector_3d a, const ULK_vector_3d b);
void  ULK_vector_3d_zero(ULK_vector_3d out);

void  ULK_vector_4d_add(ULK_vector_4d out, const ULK_vector_4d a, const ULK_vector_4d b);
float ULK_vector_4d_angle(const ULK_vector_4d a, const ULK_vector_4d b);
void  ULK_vector_4d_copy(ULK_vector_4d dst, const ULK_vector_4d src);
float ULK_vector_4d_dist(const ULK_vector_4d a, const ULK_vector_4d b);
float ULK_vector_4d_dist2(const ULK_vector_4d a, const ULK_vector_4d b);
void  ULK_vector_4d_div(ULK_vector_4d out, const ULK_vector_4d in, float s);
float ULK_vector_4d_dot(const ULK_vector_4d a, const ULK_vector_4d b);
void  ULK_vector_4d_lerp(ULK_vector_4d out, const ULK_vector_4d a, const ULK_vector_4d b, float t);
float ULK_vector_4d_mag(const ULK_vector_4d in);
float ULK_vector_4d_mag2(const ULK_vector_4d in);
void  ULK_vector_4d_mul(ULK_vector_4d out, const ULK_vector_4d in, float s);
void  ULK_vector_4d_negate(ULK_vector_4d out, const ULK_vector_4d in);
void  ULK_vector_4d_norm(ULK_vector_4d out, const ULK_vector_4d in);
void  ULK_vector_4d_set(ULK_vector_4d out, float x, float y, float z, float w);
void  ULK_vector_4d_sub(ULK_vector_4d out, const ULK_vector_4d a, const ULK_vector_4d b);
void  ULK_vector_4d_zero(ULK_vector_4d out);

#endif
