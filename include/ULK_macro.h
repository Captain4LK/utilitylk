/*

██    ██ ██      ██   ██         ███    ███  █████   ██████ ██████   ██████  
██    ██ ██      ██  ██          ████  ████ ██   ██ ██      ██   ██ ██    ██ 
██    ██ ██      █████           ██ ████ ██ ███████ ██      ██████  ██    ██ 
██    ██ ██      ██  ██          ██  ██  ██ ██   ██ ██      ██   ██ ██    ██ 
 ██████  ███████ ██   ██ ███████ ██      ██ ██   ██  ██████ ██   ██  ██████  
                                                                             
*/

/* 
LICENSE OPTION A: 3-clause BSD

   Copyright (C) 2020-2021 Captain4LK (Lukas Holzbeierlein)

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

LICENSE OPTION B: Public Domain - Unlicense

   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <http://unlicense.org/>

*/

/*

██    ██ ███████  █████   ██████  ███████ 
██    ██ ██      ██   ██ ██       ██      
██    ██ ███████ ███████ ██   ███ █████   
██    ██      ██ ██   ██ ██    ██ ██      
 ██████  ███████ ██   ██  ██████  ███████ 
                                          
-----------------------------------------

1. Include ULK_macro.h in every source file you need it

Linker options: None
*/

#ifndef _ULK_MACRO_H_

#define _ULK_MACRO_H_

//Bits per byte, should be 8
#define ULK_MACRO_BPB \
   sizeof(char)
//-----------------------------------------

//Branchless MIN/MAX functions
//* only works for integers
//* might be slower than a<b?a:b
#define ULK_MACRO_IMAX(a,b) \
   (a^((a^b)&-(a<b)))

#define ULK_MACRO_IMIN(a,b) \
   (b^((a^b)&-(a<b)))
//-----------------------------------------

//Branchless MIN/MAX functions
//* quicker than ULK_MACRO_IMAX
//* only works if INT_MIN<=a-b<=INT_MAX (most of the time basically)
#define ULK_MACRO_IQMAX(a,b) \
   (a^((a^b)&-(a<b)))

#define ULK_MACRO_IQMIN(a,b) \
   (b+((b-a)&((b-a)>>(sizeof(int)*ULK_MACRO_BPB-1))))
//-----------------------------------------

#endif
