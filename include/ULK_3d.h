/*

██    ██ ██      ██   ██         ██████  ██████  
██    ██ ██      ██  ██               ██ ██   ██ 
██    ██ ██      █████            █████  ██   ██ 
██    ██ ██      ██  ██               ██ ██   ██ 
 ██████  ███████ ██   ██ ███████ ██████  ██████  
                                                 
*/                                                 

/* 
LICENSE OPTION A: 3-clause BSD

   Copyright (C) 2020-2021 Captain4LK (Lukas Holzbeierlein)

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

LICENSE OPTION B: Public Domain - Unlicense

   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <http://unlicense.org/>
*/

/*

██    ██ ███████  █████   ██████  ███████ 
██    ██ ██      ██   ██ ██       ██      
██    ██ ███████ ███████ ██   ███ █████   
██    ██      ██ ██   ██ ██    ██ ██      
 ██████  ███████ ██   ██  ██████  ███████ 
                                          
-----------------------------------------

1. Add ULK_3d.c, ULK_matrix.c and ULK_vector.c to your source files.
2. Add ULK_3d.h to your include files.
3. Edit ULK_3d.c, l.55 to match the path to ULK_3d.h
4. Edit ULK_matrix.c, l.62 to match the path to ULK_matrix.h.
5. Edit ULK_vector.c, l.64 to match the path to ULK_vector.h.

Linker options: None, unless your plattform treats math.h as a library, then add -lm (eg. Linux).
*/

#ifndef _ULK_3D_H_

#define _ULK_3D_H_

#include "ULK_vector.h"
#include "ULK_matrix.h"

typedef struct
{
   float a;
   float b;
   float c;
   float d;
}ULK_plane;

typedef struct ULK_vertex
{
   ULK_vector_3d pos;
   ULK_vector_3d pos_pr; //processed pos
   float u;
   float v;
   struct ULK_vertex *next;
}ULK_vertex;

typedef struct
{
   int num_planes;
   ULK_plane *planes[10];
}ULK_frustum;

typedef struct ULK_polygon
{
   ULK_vertex *vertices;
   ULK_plane plane;

   struct ULK_polygon *next;
}ULK_polygon;

void        ULK_plane_face(ULK_plane *plane, float x, float y, float z);
void        ULK_plane_face_vector(ULK_plane *plane, const ULK_vector_3d pos);
int         ULK_plane_facing(const ULK_plane *plane, float x, float y, float z);
int         ULK_plane_facing_vector(const ULK_plane *plane, const ULK_vector_3d pos);
void        ULK_plane_from_pos(ULK_plane *plane, const ULK_vector_3d pos0, const ULK_vector_3d pos1, const ULK_vector_3d pos2);
void        ULK_plane_from_polygon(ULK_plane *plane, ULK_polygon *poly);
void        ULK_plane_normalize(ULK_plane *out, const ULK_plane *in);
void        ULK_plane_reset(ULK_plane *plane);
void        ULK_plane_sigswap(ULK_plane *plane);
void        ULK_plane_set(ULK_plane *plane, float a, float b, float c, float d);
void        ULK_plane_transform(ULK_plane *out, const ULK_plane *in, const ULK_matrix_4x4 m);
void        ULK_plane_unface(ULK_plane *plane, float x, float y, float z);
void        ULK_plane_unface_vector(ULK_plane *plane, const ULK_vector_3d pos);

ULK_vertex *ULK_vertex_clip(ULK_vertex *poly, const ULK_plane *plane, int processed);
void        ULK_vertex_copy(ULK_vertex *dst, const ULK_vertex *src);
void        ULK_vertex_reset_temp();
int         ULK_vertex_count(ULK_vertex *vertex);
int         ULK_vertex_winding(ULK_vertex *vertex, int processed);

void        ULK_frustum_add_plane(ULK_frustum *frustum, ULK_plane *plane);
ULK_vertex *ULK_frustum_clip(const ULK_frustum *frustum, ULK_vertex *verts, int processed);
void        ULK_frustum_create(ULK_frustum *out, const ULK_matrix_4x4 projection);
ULK_plane  *ULK_frustum_get_plane(const ULK_frustum *frustum, int plane);
int         ULK_frustum_get_plane_num(const ULK_frustum *frustum);
void        ULK_frustum_set_plane(ULK_frustum *frustum, int planes);

//Vector extension
float       ULK_vector_3d_distance_plane(const ULK_vector_3d a, const ULK_plane *b);

#endif
