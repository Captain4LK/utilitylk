/*

██    ██ ██      ██   ██         ███    ███  █████  ████████ ██████  ██ ██   ██ 
██    ██ ██      ██  ██          ████  ████ ██   ██    ██    ██   ██ ██  ██ ██  
██    ██ ██      █████           ██ ████ ██ ███████    ██    ██████  ██   ███   
██    ██ ██      ██  ██          ██  ██  ██ ██   ██    ██    ██   ██ ██  ██ ██  
 ██████  ███████ ██   ██ ███████ ██      ██ ██   ██    ██    ██   ██ ██ ██   ██ 
                                                                                
*/

/* 
LICENSE OPTION A: 3-clause BSD

   Copyright (C) 2020-2021 Captain4LK (Lukas Holzbeierlein)

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

LICENSE OPTION B: Public Domain - Unlicense

   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <http://unlicense.org/>
*/

/*

██    ██ ███████  █████   ██████  ███████ 
██    ██ ██      ██   ██ ██       ██      
██    ██ ███████ ███████ ██   ███ █████   
██    ██      ██ ██   ██ ██    ██ ██      
 ██████  ███████ ██   ██  ██████  ███████ 
                                          
-----------------------------------------

1. Add ULK_matrix.c and ULK_vector.c to your source files.
2. Add ULK_matrix.h to your include files.
3. Edit ULK_matrix.c, l.65 to match the path to ULK_matrix.h.
4. Edit ULK_vector.c, l.64 to match the path to ULK_vector.h.

Linker options: None, unless your plattform treats math.h as a library, then add -lm (eg. Linux).
*/

#ifndef _ULK_MATRIX_H_

#define _ULK_MATRIX_H_

#include "ULK_vector.h"

typedef float ULK_matrix_2x2[4];
typedef float ULK_matrix_3x3[9];
typedef float ULK_matrix_4x4[16];

void  ULK_matrix_2x2_add(ULK_matrix_2x2 out, const ULK_matrix_2x2 a, const ULK_matrix_2x2 b);
void  ULK_matrix_2x2_copy(ULK_matrix_2x2 dst, const ULK_matrix_2x2 src);
float ULK_matrix_2x2_determinant(const ULK_matrix_2x2 in);
void  ULK_matrix_2x2_identity(ULK_matrix_2x2 out);
void  ULK_matrix_2x2_inverse(ULK_matrix_2x2 out, const ULK_matrix_2x2 in);
void  ULK_matrix_2x2_mul(ULK_matrix_2x2 out, const ULK_matrix_2x2 a, const ULK_matrix_2x2 b);
void  ULK_matrix_2x2_mul_vector_2d(ULK_vector_2d out, const ULK_matrix_2x2 a, const ULK_vector_2d b);
void  ULK_matrix_2x2_rotate(ULK_matrix_2x2 out, const ULK_matrix_2x2 in, float angle_rad);
void  ULK_matrix_2x2_scale(ULK_matrix_2x2 out, const ULK_matrix_2x2 in, float scale);
void  ULK_matrix_2x2_set_rotation(ULK_matrix_2x2 out, float angle_rad);
void  ULK_matrix_2x2_set_scale(ULK_matrix_2x2 out, float scale);
void  ULK_matrix_2x2_sub(ULK_matrix_2x2 out, const ULK_matrix_2x2 a, const ULK_matrix_2x2 b);
void  ULK_matrix_2x2_transpose(ULK_matrix_2x2 out, const ULK_matrix_2x2 in);
void  ULK_matrix_2x2_zero(ULK_matrix_2x2 out);
 
void  ULK_matrix_3x3_add(ULK_matrix_3x3 out, const ULK_matrix_3x3 a, const ULK_matrix_3x3 b);
void  ULK_matrix_3x3_copy(ULK_matrix_3x3 dst, const ULK_matrix_3x3 src);
float ULK_matrix_3x3_determinant(const ULK_matrix_3x3 in);
void  ULK_matrix_3x3_identity(ULK_matrix_3x3 out);
void  ULK_matrix_3x3_inverse(ULK_matrix_3x3 out, const ULK_matrix_3x3 in);
void  ULK_matrix_3x3_mul(ULK_matrix_3x3 out, const ULK_matrix_3x3 a, const ULK_matrix_3x3 b);
void  ULK_matrix_3x3_mul_vector_3d(ULK_vector_3d out, const ULK_matrix_3x3 a, const ULK_vector_3d b);
void  ULK_matrix_3x3_rotate_2d(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float angle_rad);
void  ULK_matrix_3x3_rotate_x(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float angle_rad);
void  ULK_matrix_3x3_rotate_y(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float angle_rad);
void  ULK_matrix_3x3_rotate_z(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float angle_rad);
void  ULK_matrix_3x3_scale(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float scale);
void  ULK_matrix_3x3_scale_2d(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float scale);
void  ULK_matrix_3x3_set_rotation_2d(ULK_matrix_3x3 out, float angle_rad);
void  ULK_matrix_3x3_set_rotation_x(ULK_matrix_3x3 out, float angle_rad);
void  ULK_matrix_3x3_set_rotation_y(ULK_matrix_3x3 out, float angle_rad);
void  ULK_matrix_3x3_set_rotation_z(ULK_matrix_3x3 out, float angle_rad);
void  ULK_matrix_3x3_set_scale(ULK_matrix_3x3 out, float scale);
void  ULK_matrix_3x3_set_scale_2d(ULK_matrix_3x3 out, float scale);
void  ULK_matrix_3x3_set_translation(ULK_matrix_3x3 out, const ULK_vector_2d trans);
void  ULK_matrix_3x3_sub(ULK_matrix_3x3 out, const ULK_matrix_3x3 a, const ULK_matrix_3x3 b);
void  ULK_matrix_3x3_translate(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, const ULK_vector_2d trans);
void  ULK_matrix_3x3_transpose(ULK_matrix_3x3 out, const ULK_matrix_3x3 in);
void  ULK_matrix_3x3_zero(ULK_matrix_3x3 out);

void  ULK_matrix_4x4_add(ULK_matrix_4x4 out, const ULK_matrix_4x4 a, const ULK_matrix_4x4 b);
void  ULK_matrix_4x4_copy(ULK_matrix_4x4 dst, const ULK_matrix_4x4 src);
void  ULK_matrix_4x4_identity(ULK_matrix_4x4 out);
void  ULK_matrix_4x4_inverse(ULK_matrix_4x4 out, const ULK_matrix_4x4 in);
void  ULK_matrix_4x4_look_at(ULK_matrix_4x4 out, ULK_vector_3d from, ULK_vector_3d to, ULK_vector_3d up);
void  ULK_matrix_4x4_mul(ULK_matrix_4x4 out, const ULK_matrix_4x4 a, const ULK_matrix_4x4 b);
void  ULK_matrix_4x4_mul_vector_3d(ULK_vector_3d out, const ULK_matrix_4x4 a, const ULK_vector_3d b);
void  ULK_matrix_4x4_mul_vector_4d(ULK_vector_4d out, const ULK_matrix_4x4 a, const ULK_vector_4d b);
void  ULK_matrix_4x4_ortho(ULK_matrix_4x4 out, float left, float right, float bottom, float top, float near, float far);
void  ULK_matrix_4x4_perspective(ULK_matrix_4x4 out, float angle, float aspect, float near, float far);
void  ULK_matrix_4x4_rotate(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, const ULK_vector_3d rot_axis, float angle_rad);
void  ULK_matrix_4x4_rotate_x(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, float angle_rad);
void  ULK_matrix_4x4_rotate_y(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, float angle_rad);
void  ULK_matrix_4x4_rotate_z(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, float angle_rad);
void  ULK_matrix_4x4_scale(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, float scale);
void  ULK_matrix_4x4_scale_3d(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, ULK_vector_3d scale);
void  ULK_matrix_4x4_set_scale(ULK_matrix_4x4 out, float scale);
void  ULK_matrix_4x4_set_scale_3d(ULK_matrix_4x4 out, ULK_vector_3d scale);
void  ULK_matrix_4x4_set_rotation(ULK_matrix_4x4 out, const ULK_vector_3d rot_axis, float angle_rad);
void  ULK_matrix_4x4_set_rotation_x(ULK_matrix_4x4 out, float angle_rad);
void  ULK_matrix_4x4_set_rotation_y(ULK_matrix_4x4 out, float angle_rad);
void  ULK_matrix_4x4_set_rotation_z(ULK_matrix_4x4 out, float angle_rad);
void  ULK_matrix_4x4_set_translation(ULK_matrix_4x4 out, const ULK_vector_3d trans);
void  ULK_matrix_4x4_sub(ULK_matrix_4x4 out, const ULK_matrix_4x4 a, const ULK_matrix_4x4 b);
void  ULK_matrix_4x4_translate(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, const ULK_vector_3d trans);
void  ULK_matrix_4x4_transpose(ULK_matrix_4x4 out, const ULK_matrix_4x4 in);
void  ULK_matrix_4x4_zero(ULK_matrix_4x4 out);

#endif
