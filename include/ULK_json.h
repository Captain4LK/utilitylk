/*

██    ██ ██      ██   ██              ██ ███████  ██████  ███    ██ 
██    ██ ██      ██  ██               ██ ██      ██    ██ ████   ██ 
██    ██ ██      █████                ██ ███████ ██    ██ ██ ██  ██ 
██    ██ ██      ██  ██          ██   ██      ██ ██    ██ ██  ██ ██ 
 ██████  ███████ ██   ██ ███████  █████  ███████  ██████  ██   ████ 
                                                                    
*/

/* 
LICENSE OPTION A: 3-clause BSD

   Copyright (C) 2020-2021 Captain4LK (Lukas Holzbeierlein)

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

LICENSE OPTION B: Public Domain - Unlicense

   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <http://unlicense.org/>
*/

/*

██    ██ ███████  █████   ██████  ███████ 
██    ██ ██      ██   ██ ██       ██      
██    ██ ███████ ███████ ██   ███ █████   
██    ██      ██ ██   ██ ██    ██ ██      
 ██████  ███████ ██   ██  ██████  ███████ 
                                          
-----------------------------------------

1. Add ULK_json.c to your source files.
2. Add ULK_json.h to your include files.
3. Edit ULK_json.c, l.71 to match the path to ULK_json.h.

Linker options: None, unless your plattform treats math.h as a library, then add -lm (eg. Linux).
*/

#ifndef _ULK_JSON_H_

#define _ULK_JSON_H_

#include <stdio.h>
#include <stdint.h>

typedef enum ULK_json5_type 
{
   ULK_json5_undefined,
   ULK_json5_null,
   ULK_json5_bool,
   ULK_json5_object,
   ULK_json5_string,
   ULK_json5_array,
   ULK_json5_integer,
   ULK_json5_real,
}ULK_json5_type;

typedef struct ULK_json5
{
   char *name;
   unsigned type:3;
   unsigned count:29;
   union 
   {
      struct ULK_json5* array;
      struct ULK_json5* nodes;
      int64_t integer;
      double real;
      char *string;
      int boolean;
   };
}ULK_json5;

typedef struct
{
   char *data;
   size_t data_size;
   ULK_json5 root;
}ULK_json5_root;

ULK_json5_root *ULK_json_parse_file(const char *path);
ULK_json5_root *ULK_json_parse_file_stream(FILE *f);
ULK_json5_root *ULK_json_parse_char_buffer(char *buffer, size_t size); //buffer must be allocated by user, but free by ULK_json
void            ULK_json_write_file(FILE *f, ULK_json5 *j);
void            ULK_json_free(ULK_json5_root *r);

//JSON file creation
//ALL char * MUST be persistend until ULK_json_write_file function call
ULK_json5_root *ULK_json_create_root();
ULK_json5       ULK_json_create_object();
ULK_json5       ULK_json_create_array();
void            ULK_json_object_add_string(ULK_json5 *j, char *name, char *value);
void            ULK_json_object_add_real(ULK_json5 *j, char *name, double value);
void            ULK_json_object_add_integer(ULK_json5 *j, char *name, int64_t value);
void            ULK_json_object_add_boolean(ULK_json5 *j, char *name, int value);
void            ULK_json_object_add_object(ULK_json5 *j, char *name, ULK_json5 o); 
void            ULK_json_object_add_array(ULK_json5 *j, char *name, ULK_json5 a); 
void            ULK_json_array_add_string(ULK_json5 *a, char *value);
void            ULK_json_array_add_real(ULK_json5 *a, double value);
void            ULK_json_array_add_integer(ULK_json5 *a, int64_t value);
void            ULK_json_array_add_boolean(ULK_json5 *a, int value);
void            ULK_json_array_add_object(ULK_json5 *a, ULK_json5 o); 
void            ULK_json_array_add_array(ULK_json5 *a, ULK_json5 ar); 

ULK_json5      *ULK_json_get_object(ULK_json5 *json, const char *name);
ULK_json5      *ULK_json_get_array_item(ULK_json5 *json, int index);
int             ULK_json_get_array_size(const ULK_json5 *json);

//Save access methods
//Returns the value of the variable if it exists
//If the variable does not exist or is of a different type, 
//the function will return the fallback value
//If you request an integer/real and the variable is of type real/integer
//the value will be converted
char           *ULK_json_get_object_string(ULK_json5 *json, const char *name, char *fallback);
double          ULK_json_get_object_real(ULK_json5 *json, const char *name, double fallback);
int64_t         ULK_json_get_object_integer(ULK_json5 *json, const char *name, int64_t fallback);
int             ULK_json_get_object_boolean(ULK_json5 *json, const char *name, int fallback);
ULK_json5      *ULK_json_get_object_object(ULK_json5 *json, const char *name, ULK_json5 *fallback);
char           *ULK_json_get_array_string(ULK_json5 *json, int index, char *fallback);
double          ULK_json_get_array_real(ULK_json5 *json, int index, double fallback);
int64_t         ULK_json_get_array_integer(ULK_json5 *json, int index, int64_t fallback);
int             ULK_json_get_array_boolean(ULK_json5 *json, int index, int fallback);
ULK_json5      *ULK_json_get_array_object(ULK_json5 *json, int index, ULK_json5 *fallback);

#endif
