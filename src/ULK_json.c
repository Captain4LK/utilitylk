/* 
LICENSE OPTION A: 3-clause BSD

   Copyright (C) 2020-2021 Captain4LK (Lukas Holzbeierlein)

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

LICENSE OPTION B: Public Domain - Unlicense

   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <http://unlicense.org/>
*/

//Based on tinyjson5 by r-yleh (https://github.com/r-lyeh/tinybits/blob/master/tinyjson5.c)

/*
Changelog:
   v0.2:
      - added ULK_json_free()
   v1.0:
      - added json creation and saving
   v1.1:
      - added ULK_json_parse_file_stream
   v1.2:
      - added ULK_json_parse_char_buffer
   v1.3:
      - added save access functions
*/

//External includes
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
//-------------------------------------

//Internal includes
#include "../include/ULK_json.h"
//-------------------------------------

//#defines
#ifndef ULK_JSON5_REALLOC
#define ULK_JSON5_REALLOC realloc
#endif

//array library
#ifndef array_cast
#ifdef __cplusplus
#define array_cast(x) (decltype(x))
#else
#define array_cast(x) (void *)
#endif
#define array_push(t, ...) ( (t) = array_cast(t) vrealloc((t), (array_count(t) + 1) * sizeof(0[t]) ), (t)[ array_count(t) - 1 ] = (__VA_ARGS__) )
#define array_count(t) (int)( (t) ? vsize(t) / sizeof(0[t]) : 0u )
#define array_free(t) ( array_cast(t) vrealloc((t), 0), (t) = 0 )
#endif

#ifndef JSON5_ASSERT
#define JSON5_ASSERT do { printf("JSON5: Error L%d while parsing '%c' in '%.16s'\n", __LINE__, p[0], p); assert(0); } while(0)
#endif
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
//-------------------------------------

//Function prototypes
static char *json5_parse(ULK_json5 *root, char *source, int flags);
static void json5_write(FILE *f, const ULK_json5 *o,int indent);
static void json5_free(ULK_json5 *root);
static char *json5__parse_value(ULK_json5 *obj, char *p, char **err_code);
static size_t vsize(void *p);
static void *vresize(void *p, size_t sz);
static void *vrealloc(void *p, size_t sz);
static char *json5__trim(char *p);
static char *json5__parse_string(ULK_json5 *obj, char *p, char **err_code);
static char *json5__parse_object(ULK_json5 *obj, char *p, char **err_code);
//-------------------------------------

//Function implementations

ULK_json5_root *ULK_json_parse_file(const char *path)
{
   //Load data from file
   ULK_json5_root *r = ULK_JSON5_REALLOC(NULL,sizeof(*r));
   memset(r,0,sizeof(*r));
   FILE *f = fopen(path,"r");
   fseek(f,0,SEEK_END);
   r->data_size = ftell(f);
   rewind(f);
   r->data = ULK_JSON5_REALLOC(NULL,sizeof(*r->data)*(r->data_size+1));
   fread(r->data,r->data_size,1,f);
   r->data[r->data_size] = '\0';
   fclose(f);

   //Parsing
   json5_parse(&r->root,r->data,0);

   return r;
}

ULK_json5_root *ULK_json_parse_file_stream(FILE *f)
{
   //Load data from file
   ULK_json5_root *r = ULK_JSON5_REALLOC(NULL,sizeof(*r));
   memset(r,0,sizeof(*r));
   fseek(f,0,SEEK_END);
   r->data_size = ftell(f);
   rewind(f);
   r->data = ULK_JSON5_REALLOC(NULL,sizeof(*r->data)*(r->data_size+1));
   fread(r->data,r->data_size,1,f);
   r->data[r->data_size] = '\0';

   //Parsing
   json5_parse(&r->root,r->data,0);

   return r;
}

ULK_json5_root *ULK_json_parse_char_buffer(char *buffer, size_t size)
{
   ULK_json5_root *r = ULK_JSON5_REALLOC(NULL,sizeof(*r));
   memset(r,0,sizeof(*r));
   r->data_size = size;
   r->data = buffer;

   //Parsing
   json5_parse(&r->root,r->data,0);

   return r;
}

void ULK_json_write_file(FILE *f, ULK_json5 *j)
{
   json5_write(f,j,0);
}

void ULK_json_free(ULK_json5_root *r)
{
   r->data = realloc(r->data,0);
   json5_free(&r->root);
}

ULK_json5_root *ULK_json_create_root()
{
   ULK_json5_root *r = ULK_JSON5_REALLOC(NULL,sizeof(*r));
   r->data = NULL;
   r->data_size = 0;
   r->root.name = NULL;
   r->root.type = ULK_json5_object;
   r->root.count = 0;
   r->root.nodes = NULL;

   return r;
}

ULK_json5 ULK_json_create_object()
{
   ULK_json5 j = {0}; 
   j.type = ULK_json5_object;
   j.count = 0;
   j.nodes = NULL;

   return j;
}

ULK_json5 ULK_json_create_array()
{
   ULK_json5 j = {0}; 
   j.type = ULK_json5_array;
   j.count = 0;
   j.array = NULL;

   return j;
}

void ULK_json_object_add_string(ULK_json5 *j, char *name, char *value)
{
   ULK_json5 node = {0};
   node.name = name;
   node.type = ULK_json5_string;
   node.string = value;

   array_push(j->nodes,node);
   ++j->count;
}

void ULK_json_object_add_real(ULK_json5 *j, char *name, double value)
{
   ULK_json5 node = {0};
   node.name = name;
   node.type = ULK_json5_real;
   node.real = value;

   array_push(j->nodes,node);
   ++j->count;
}

void ULK_json_object_add_integer(ULK_json5 *j, char *name, int64_t value)
{
   ULK_json5 node = {0};
   node.name = name;
   node.type = ULK_json5_integer;
   node.integer = value;

   array_push(j->nodes,node);
   ++j->count;
}

void ULK_json_object_add_boolean(ULK_json5 *j, char *name, int value)
{
   ULK_json5 node = {0};
   node.name = name;
   node.type = ULK_json5_bool;
   node.boolean = value;

   array_push(j->nodes,node);
   ++j->count;
}

void ULK_json_object_add_object(ULK_json5 *j, char *name, ULK_json5 o)
{
   o.name = name;
   array_push(j->nodes,o);
   ++j->count;
}

void ULK_json_object_add_array(ULK_json5 *j, char *name, ULK_json5 a)
{
   a.name = name;
   array_push(j->nodes,a);
   ++j->count;
}

void ULK_json_array_add_string(ULK_json5 *a, char *value)
{
   ULK_json5 node = {0};
   node.name = NULL;
   node.type = ULK_json5_string;
   node.string = value;

   array_push(a->array,node);
   ++a->count;
}

void ULK_json_array_add_real(ULK_json5 *a, double value)
{
   ULK_json5 node = {0};
   node.name = NULL;
   node.type = ULK_json5_real;
   node.real = value;

   array_push(a->array,node);
   ++a->count;
}

void ULK_json_array_add_integer(ULK_json5 *a, int64_t value)
{
   ULK_json5 node = {0};
   node.name = NULL;
   node.type = ULK_json5_integer;
   node.integer = value;

   array_push(a->array,node);
   ++a->count;
}

void ULK_json_array_add_boolean(ULK_json5 *a, int value)
{
   ULK_json5 node = {0};
   node.name = NULL;
   node.type = ULK_json5_bool;
   node.boolean = value;

   array_push(a->array,node);
   ++a->count;
}

void ULK_json_array_add_object(ULK_json5 *a, ULK_json5 o)
{
   o.name = NULL;
   array_push(a->array,o);
   ++a->count;
}

void ULK_json_array_add_array(ULK_json5 *a, ULK_json5 ar)
{
   ar.name = NULL;
   array_push(a->array,ar);
   ++a->count;
}


ULK_json5 *ULK_json_get_object(ULK_json5 *json, const char *name)
{
   //Not an object
   if(json->type!=ULK_json5_object)
      return NULL;

   for(int i = 0;i<json->count;i++)
   {
      if(strcmp(name,json->nodes[i].name)==0)
         return &json->nodes[i];
   }

   //Not found
   return NULL;
}

int ULK_json_get_array_size(const ULK_json5 *json)
{
   if(json->type!=ULK_json5_array)
      return -1;

   return json->count;
}

ULK_json5 *ULK_json_get_array_item(ULK_json5 *json, int index)
{
   //Not an array
   if(json->type!=ULK_json5_array)
      return NULL;

   //Out of bounds
   if(index<0||index>=json->count)
      return NULL;

   return &json->array[index];
}

char *ULK_json_get_object_string(ULK_json5 *json, const char *name, char *fallback)
{
   if(!json||json->type!=ULK_json5_object)
      return fallback;

   ULK_json5 *o = ULK_json_get_object(json,name);
   if(!o)
      return fallback;

   if(o->type==ULK_json5_string)
      return o->string;

   return fallback;
}

double ULK_json_get_object_real(ULK_json5 *json, const char *name, double fallback)
{
   if(!json||json->type!=ULK_json5_object)
      return fallback;

   ULK_json5 *o = ULK_json_get_object(json,name);
   if(!o)
      return fallback;

   if(o->type==ULK_json5_real)
      return o->real;

   if(o->type==ULK_json5_integer)
      return (double)o->integer;

   return fallback;
}

int64_t ULK_json_get_object_integer(ULK_json5 *json, const char *name, int64_t fallback)
{
   if(!json||json->type!=ULK_json5_object)
      return fallback;

   ULK_json5 *o = ULK_json_get_object(json,name);
   if(!o)
      return fallback;

   if(o->type==ULK_json5_integer)
      return o->integer;

   if(o->type==ULK_json5_real)
      return (int64_t)o->real;

   return fallback;
}

int ULK_json_get_object_boolean(ULK_json5 *json, const char *name, int fallback)
{
   if(!json||json->type!=ULK_json5_object)
      return fallback;

   ULK_json5 *o = ULK_json_get_object(json,name);
   if(!o)
      return fallback;

   if(o->type==ULK_json5_bool)
      return o->boolean;

   return fallback;
}

ULK_json5 *ULK_json_get_object_object(ULK_json5 *json, const char *name, ULK_json5 *fallback)
{
   if(!json||json->type!=ULK_json5_object)
      return fallback;

   ULK_json5 *o = ULK_json_get_object(json,name);
   if(!o)
      return fallback;

   if(o->type==ULK_json5_object)
      return o;

   return fallback;
}

char *ULK_json_get_array_string(ULK_json5 *json, int index, char *fallback)
{
   if(!json||json->type!=ULK_json5_array)
      return fallback;

   ULK_json5 *o = ULK_json_get_array_item(json,index);
   if(!o)
      return fallback;

   if(o->type==ULK_json5_string)
      return o->string;

   return fallback;
}

double ULK_json_get_array_real(ULK_json5 *json, int index, double fallback)
{
   if(!json||json->type!=ULK_json5_array)
      return fallback;

   ULK_json5 *o = ULK_json_get_array_item(json,index);
   if(!o)
      return fallback;

   if(o->type==ULK_json5_real)
      return o->real;

   if(o->type==ULK_json5_integer)
      return (double)o->integer;

   return fallback;
}

int64_t ULK_json_get_array_integer(ULK_json5 *json, int index, int64_t fallback)
{
   if(!json||json->type!=ULK_json5_array)
      return fallback;

   ULK_json5 *o = ULK_json_get_array_item(json,index);
   if(!o)
      return fallback;

   if(o->type==ULK_json5_integer)
      return o->integer;

   if(o->type==ULK_json5_real)
      return (int64_t)o->real;

   return fallback;
}

int ULK_json_get_array_boolean(ULK_json5 *json, int index, int fallback)
{
   if(!json||json->type!=ULK_json5_array)
      return fallback;

   ULK_json5 *o = ULK_json_get_array_item(json,index);
   if(!o)
      return fallback;

   if(o->type==ULK_json5_bool)
      return o->boolean;

   return fallback;
}

ULK_json5 *ULK_json_get_array_object(ULK_json5 *json, int index, ULK_json5 *fallback)
{
   if(!json||json->type!=ULK_json5_array)
      return fallback;

   ULK_json5 *o = ULK_json_get_array_item(json,index);
   if(!o)
      return fallback;

   if(o->type==ULK_json5_object)
      return o;

   return fallback;
}


//tinyjson5 code:

// JSON5 + SJSON parser module
//
// License:
// This software is dual-licensed to the public domain and under the following
// license: you are granted a perpetual, irrevocable license to copy, modify,
// publish, and distribute this file as you see fit.
// No warranty is implied, use at your own risk.
//
// Credits:
// Dominik Madarasz (original code) (GitHub: zaklaus)
// r-lyeh (fork)

// vector library -------------------------------------------------------------
static size_t vsize(void *p) 
{
   size_t sz = p?0[(size_t*)p-2]:0;
   return sz;
}

static void *vresize(void *p, size_t sz) 
{
   size_t *ret = (size_t*)p-2;
   if(!p) 
   {
      ret = (size_t*)ULK_JSON5_REALLOC( 0, sizeof(size_t) * 2 + sz );
      ret[0] = sz;
      ret[1] = 0;
   }
   else
   {
      size_t osz = ret[0];
      size_t ocp = ret[1];
      if(sz<=(osz+ocp)) 
      {
         ret[0] = sz;
         ret[1] = ocp - (sz - osz);
      }
      else
      {
         ret = (size_t *)ULK_JSON5_REALLOC(ret,sizeof(size_t)*2+sz*1.75);
         ret[0] = sz;
         ret[1] = (size_t)(sz*1.75)-sz;
      }
   }

   return &ret[2];
}

static void *vrealloc(void *p, size_t sz)
{
   if(sz) 
   {
      return vresize( p, sz );
   }
   else
   {
      if(p) 
      {
         size_t *ret = (size_t*)p-2;
         ret[0] = 0;
         ret[1] = 0;
         ret = ULK_JSON5_REALLOC(ret,0);
      }
      return 0;
   }
}

// json5 ----------------------------------------------------------------------
static char *json5__trim(char *p)
{
   while (*p) 
   {
      if(isspace(*p)) 
      {
         ++p;
      }
      else if(p[0]=='/'&&p[1]=='*') 
      { 
         //skip C comment
         for(p+=2;*p&&!(p[0]=='*'&&p[1]=='/');++p);
         if(*p) 
            p+=2;
      }
      else if(p[0]=='/'&&p[1]=='/') 
      { 
         //skip C++ comment
         for(p+=2;*p&&p[0]!='\n';++p);
         if( *p ) 
            ++p;
      }
      else 
      {
         break;
      }
   }

   return p;
}

static char *json5__parse_string(ULK_json5 *obj, char *p, char **err_code)
{
   assert(obj&&p);

   if(*p=='"'||*p=='\''||*p=='`') 
   {
      obj->type = ULK_json5_string;
      obj->string = p+1;

      char eos_char = *p;
      char *b = obj->string;
      char *e = b;
      while (*e) 
      {
         if(*e=='\\'&&(e[1]==eos_char)) 
            ++e;
         else if(*e=='\\'&&(e[1]=='\r'||e[1]=='\n')) 
            *e = ' ';
         else if(*e==eos_char) 
            break;
         ++e;
      }

      *e = '\0';
      return p = e+1;
   }

   //JSON5_ASSERT; *err_code = "json5_error_invalid_value";
   return NULL;
}

static char *json5__parse_object(ULK_json5 *obj, char *p, char **err_code)
{
   assert(obj&&p);

   if(1) /* <-- for SJSON */
   { 
      int skip = *p=='{'; /* <-- for SJSON */
      obj->type = ULK_json5_object;
      obj->nodes = NULL;

      while(*p)
      {
         ULK_json5 node = {0};
         do 
         { 
            p = json5__trim(p+skip);
            skip = 1; 
         }
         while(*p ==',');

         if(*p =='}') 
         {
            ++p;
            break;
         }
         // @todo: is_unicode() (s[0] == '\\' && isxdigit(s[1]) && isxdigit(s[2]) && isxdigit(s[3]) && isxdigit(s[4]))) {
         else if(isalpha(*p)||*p=='_'||*p=='$') 
         { 
             // also || is_unicode(p)
            node.name = p;

            do 
            {
               ++p;
            } 
            while(*p&&(*p=='_'||isalpha(*p)||isdigit(*p))); // also || is_unicode(p)

            char *e = p;
            p = json5__trim(p);
            *e = '\0';
         }
         else 
         { 
            //if( *p == '"' || *p == '\'' || *p == '`' ) {
            char *ps = json5__parse_string(&node, p, err_code);
            if(!ps) 
            {
               return NULL;
            }
            p = ps;
            node.name = node.string;
            p = json5__trim(p);
         }

         // @todo: https://www.ecma-international.org/ecma-262/5.1/#sec-7.6
         if(!(node.name && node.name[0])) 
         { 
            // !json5__validate_name(node.name) ) {
            JSON5_ASSERT; *err_code = "json5_error_invalid_name";
            return NULL;
         }

         if(!p||(*p&&(*p!=':'&&*p!='='/*<-- for SJSON */))) 
         {
            JSON5_ASSERT; *err_code = "json5_error_invalid_name";
            return NULL;
         }
         p = json5__trim(p + 1);
         p = json5__parse_value(&node, p, err_code);

         if(*err_code[0]) 
         {
            return NULL;
         }

         if(node.type!=ULK_json5_undefined) 
         {
            array_push(obj->nodes, node);
            ++obj->count;
         }

         if(*p =='}') 
         { 
            ++p; 
            break; 
         }
      }
      return p;
   }

   JSON5_ASSERT; *err_code = "json5_error_invalid_value";
   return NULL;
}

static char *json5__parse_value(ULK_json5 *obj, char *p, char **err_code) 
{
   assert(obj&&p);

   p = json5__trim(p);

   char *is_string = json5__parse_string(obj,p,err_code);

   if(is_string) 
   {
      p = is_string;
      if(*err_code[0])
      {
         return NULL;
      }
   }
   else if(*p=='{') 
   {
      p = json5__parse_object(obj,p,err_code);
      if(*err_code[0]) 
      {
         return NULL;
      }
   }
   else if(*p=='[')
   {
      obj->type = ULK_json5_array;
      obj->array = NULL;

      while (*p) 
      {
         ULK_json5 elem = {0};
         elem.array = NULL;

         do
         { 
            p = json5__trim(p+1); 
         } 
         while(*p==',');

         if(*p ==']') 
         { 
            ++p; 
            break; 
         }

         p = json5__parse_value(&elem,p,err_code);

         if(*err_code[0]) 
         {
            return NULL;
         }

         if(elem.type!=ULK_json5_undefined) 
         {
            array_push(obj->array, elem);
            ++obj->count;
         }
         if(*p==']') 
         { 
            ++p; 
            break; 
         }
      }
   }
   else if(isalpha(*p)||(*p=='-'&&!isdigit(p[1]))) 
   {
      const char *labels[] = { "null", "on","true", "off","false", "nan","NaN", "-nan","-NaN", "inf","Infinity", "-inf","-Infinity" };
      const int lenghts[] = { 4, 2,4, 3,5, 3,3, 4,4, 3,8, 4,9 };
      for(int i = 0;labels[i];++i)
      {
         if(!strncmp(p,labels[i],lenghts[i])) 
         {
            p += lenghts[i];
#ifdef _MSC_VER // somehow, NaN is apparently signed in MSC
            if(i>=5) 
            {
               obj->type = json5_real;
               obj->real = i>=11?-INFINITY:i>=9?INFINITY:i>=7?NAN:-NAN;
            }
#else
            if(i>=5)
            {
               obj->type = ULK_json5_real;
               obj->real = i>=11?-INFINITY:i>=9?INFINITY:i>=7?-NAN:NAN;
            }
#endif
            else if(i>=1) 
            {
               obj->type = ULK_json5_bool;
               obj->boolean = i <= 2;
            }
            else
            {
               obj->type = ULK_json5_null;
            }
            break;
         }
      }
      if(obj->type==ULK_json5_undefined ) 
      {
         JSON5_ASSERT; *err_code = "json5_error_invalid_value";
         return NULL;
      }
   }
   else if(isdigit(*p)||*p=='+'||*p=='-'||*p=='.')
   {
      char buffer[16] = {0};
      char *buf = buffer;
      char is_hex = 0;
      char is_dbl = 0;
      while(*p&&strchr("+-.xX0123456789aAbBcCdDeEfF",*p)) 
      {
         is_hex |= (*p | 32) == 'x';
         is_dbl |= *p == '.';
         *buf++ = *p++;
      }
      obj->type = is_dbl?ULK_json5_real:ULK_json5_integer;
      if(is_dbl) 
         sscanf(buffer,"%lf",&obj->real);
      else if(is_hex) 
         sscanf(buffer,"%lx",&obj->integer); // SCNx64 -> inttypes.h
      else
         sscanf(buffer,"%ld",&obj->integer); // SCNd64 -> inttypes.h
   }
   else 
   {
      return NULL;
   }

   return p;
}

static char *json5_parse(ULK_json5 *root, char *p, int flags) 
{
   assert(root&&p);

   char *err_code = "";
   *root = (ULK_json5) {0};

   p = json5__trim(p);
   if(*p=='[') 
   { /* <-- for SJSON */
      json5__parse_value(root, p, &err_code);
   } 
   else 
   {
      json5__parse_object(root,p,&err_code); /* <-- for SJSON */
   }

   return err_code[0] ? err_code : 0;
}

static void json5_free(ULK_json5 *root) 
{
   if(root->type==ULK_json5_array&&root->array) 
   {
      for(int i = 0, cnt = array_count(root->array);i<cnt;++i) 
      {
         json5_free(root->array + i);
      }
      array_free(root->array);
   } 

   if(root->type==ULK_json5_object&&root->nodes) 
   {
      for(int i = 0,cnt = array_count(root->nodes);i<cnt;++i) 
      {
         json5_free(root->nodes+i);
      }
      array_free(root->nodes);
   }

   *root = (ULK_json5) {0}; // needed?
}

static void json5_write(FILE *f, const ULK_json5 *o,int indent)
{
   static const char *tabs = 
   "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
   "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
   if(o->name)
   {
      fprintf(f,"%.*s\"%s\":",indent,tabs,o->name);
   }

   if(o->type==ULK_json5_null)
      fprintf(f,"%s","null");
   else if(o->type==ULK_json5_bool)
      fprintf(f,"%s",o->boolean?"true":"false");
   else if(o->type==ULK_json5_integer) 
      fprintf(f,"%ld",o->integer);
   else if(o->type==ULK_json5_real) 
   {
      if(isnan(o->real)) 
         fprintf(f,"%s",signbit(o->real)?"-nan":"nan");
      else if(isinf(o->real)) 
         fprintf(f,"%s",signbit(o->real)?"-inf":"inf");
      else 
         fprintf(f,"%.4lf",o->real);
   }
   else if(o->type==ULK_json5_string) 
   {
      // write (escaped) string
      char chars[] = "\\\"\n\r\b\f\v";
      char remap[] = "\\\"nrbfv";
      char esc[256];
      for(int i = 0;chars[i];++i) 
         esc[(unsigned)chars[i]] = remap[i];

      const char *b = o->string;
      const char *e = strpbrk(b, chars);
      const char *sep = "\"";
      while(e) 
      {
         fprintf(f,"%s%.*s\\%c",sep,(int)(e-b),b,esc[(unsigned char)*e]);
         e = strpbrk( b = e + 1, chars);
         sep = "";
      }
      fprintf(f, "%s%s\"", sep, b);
   }
   else if(o->type==ULK_json5_array) 
   {
      const char *sep = "";
      fprintf(f, "%s", "[ ");
      for(int i = 0, cnt = o->count; i < cnt; ++i ) 
      {
         fprintf(f, "%s", sep); sep = ", ";
         json5_write(f, o->array + i,indent+1);
      }
      fprintf(f, "%s", " ]");
   }
   else if(o->type==ULK_json5_object) 
   {
      const char *sep = "";
      fprintf(f, "%.*s{\n", 0 * (++indent), tabs);
      for( int i = 0, cnt = o->count; i < cnt; ++i ) 
      {
         fprintf(f, "%s", sep); sep = ",\n";
         json5_write(f, o->nodes + i,indent+1);
      }
      fprintf(f, "\n%.*s}", --indent, tabs);
   } 
   else 
   {
      char p[16] = {0};
      JSON5_ASSERT; /* "json5_error_invalid_value"; */
   }
}
//-------------------------------------
