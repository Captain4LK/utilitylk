/* 
LICENSE OPTION A: 3-clause BSD

   Copyright (C) 2020-2021 Captain4LK (Lukas Holzbeierlein)

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

LICENSE OPTION B: Public Domain - Unlicense

   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <http://unlicense.org/>
*/

/* 
TODO:
   Add 3d vector rotation

*/

/*
Changelog:
   v1.1:
      - added ULK_vector_3d_slerp
      - removed all functions allocating memory to make library more usable
   v2.0:
      - rewrite to get rid of expat conditions of gl-matrix
   v2.1: 
      - move to macro based design (--> less code to change in case of errors)
*/

//External includes
#include <math.h>
//-------------------------------------

//Internal includes
#include "../include/ULK_vector.h"
//-------------------------------------

//#defines
#define VADD(type,dim) \
   void type##_add(type out, const type a, const type b) { for(int i = 0;i<dim;i++) out[i] = a[i]+b[i]; }

#define VANGLE(type) \
   float type##_angle(const type a, const type b) { float mag = type##_mag(a)*type##_mag(b); if(mag==0.0f) return 0.0f; return acosf(type##_dot(a,b)/mag); }

#define VCOPY(type,dim) \
   void type##_copy(type dst, const type src) { for(int i = 0;i<dim;i++) dst[i] = src[i]; }

#define VDIST(type,dim) \
   float type##_dist(const type a, const type b) { type tmp; for(int i = 0;i<dim;i++) tmp[i] = b[i]-a[i]; return type##_mag(tmp); }

#define VDIST2(type,dim) \
   float type##_dist2(const type a, const type b) { type tmp; for(int i = 0;i<dim;i++) tmp[i] = b[i]-a[i]; return type##_mag2(tmp); }

#define VDIV(type,dim) \
   void type##_div(type out, const type in, float s) { for(int i = 0;i<dim;i++) out[i] = in[i]/s; }

#define VDOT(type,dim) \
   float type##_dot(const type a, const type b) { float out = 0.0f; for(int i = 0;i<dim;i++) out+=a[i]*b[i]; return out; }

#define VLERP(type,dim) \
   void type##_lerp(type out, const type a, const type b, float t) { for(int i = 0;i<dim;i++) out[i] = a[i]+t*(b[i]-a[i]); }

#define VMAG(type,dim) \
   float type##_mag(const type in) { float mag2 = 0.0f; for(int i = 0;i<dim;i++) mag2+=in[i]*in[i]; return sqrtf(mag2); }

#define VMAG2(type,dim) \
   float type##_mag2(const type in) { float mag2 = 0.0f; for(int i = 0;i<dim;i++) mag2+=in[i]*in[i]; return mag2; }

#define VMUL(type,dim) \
   void type##_mul(type out, const type in, float s) { for(int i = 0;i<dim;i++) out[i] = in[i]*s; }

#define VNEGATE(type,dim) \
   void type##_negate(type out, const type in) { for(int i = 0;i<dim;i++) out[i] = -in[i]; }

#define VNORM(type,dim) \
   void type##_norm(type out, const type in) { float mag2 = type##_mag2(in); if(mag2!=0.0f) mag2 = 1.0f/sqrtf(mag2); for(int i = 0;i<dim;i++) out[i] = in[i]*mag2; }

#define VSUB(type,dim) \
   void type##_sub(type out, const type a, const type b) { for(int i = 0;i<dim;i++) out[i] = a[i]-b[i]; }

#define VZERO(type,dim) \
   void type##_zero(type out) { for(int i = 0;i<dim;i++) out[i] = 0.0f; }
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
//-------------------------------------

//Function prototypes
//-------------------------------------

//Function implementations

VADD(ULK_vector_2d,2)
VANGLE(ULK_vector_2d)
VCOPY(ULK_vector_2d,2)
VDIST(ULK_vector_2d,2)
VDIST2(ULK_vector_2d,2)
VDIV(ULK_vector_2d,2)
VDOT(ULK_vector_2d,2)
VLERP(ULK_vector_2d,2)
VMAG(ULK_vector_2d,2)
VMAG2(ULK_vector_2d,2)
VMUL(ULK_vector_2d,2)
VNEGATE(ULK_vector_2d,2)
VNORM(ULK_vector_2d,2)
VSUB(ULK_vector_2d,2)
VZERO(ULK_vector_2d,2)

void ULK_vector_2d_rot(ULK_vector_2d out, const ULK_vector_2d in, const ULK_vector_2d origin, float radians)
{
   float dist0 = in[0]-origin[0];
   float dist1 = in[1]-origin[1];
   float sin_ang = sinf(radians);
   float cos_ang = cosf(radians);

   out[0] = dist0*cos_ang-dist1*sin_ang+origin[0];
   out[1] = dist0*sin_ang+dist1*cos_ang+origin[1];
}

void ULK_vector_2d_set(ULK_vector_2d out, float x, float y)
{
   out[0] = x;
   out[1] = y;
}

VADD(ULK_vector_3d,3)
VANGLE(ULK_vector_3d)
VCOPY(ULK_vector_3d,3)
VDIST(ULK_vector_3d,3)
VDIST2(ULK_vector_3d,3)
VDIV(ULK_vector_3d,3)
VDOT(ULK_vector_3d,3)
VLERP(ULK_vector_3d,3)
VMAG(ULK_vector_3d,3)
VMAG2(ULK_vector_3d,3)
VMUL(ULK_vector_3d,3)
VNEGATE(ULK_vector_3d,3)
VNORM(ULK_vector_3d,3)
VSUB(ULK_vector_3d,3)
VZERO(ULK_vector_3d,3)

void ULK_vector_3d_cross(ULK_vector_3d out, const ULK_vector_3d a, const ULK_vector_3d b)
{
   float a0 = a[0];
   float a1 = a[1];
   float a2 = a[2];
   float b0 = b[0];
   float b1 = b[1];
   float b2 = b[2];
      
   out[0] = a1*b2-a2*b1;
   out[1] = a2*b0-a0*b2;
   out[2] = a0*b1-a1*b0;
}

void ULK_vector_3d_set(ULK_vector_3d out, float x, float y, float z)
{
   out[0] = x;
   out[1] = y;
   out[2] = z;
}

VADD(ULK_vector_4d,4)
VANGLE(ULK_vector_4d)
VCOPY(ULK_vector_4d,4)
VDIST(ULK_vector_4d,4)
VDIST2(ULK_vector_4d,4)
VDIV(ULK_vector_4d,4)
VDOT(ULK_vector_4d,4)
VLERP(ULK_vector_4d,4)
VMAG(ULK_vector_4d,4)
VMAG2(ULK_vector_4d,4)
VMUL(ULK_vector_4d,4)
VNEGATE(ULK_vector_4d,4)
VNORM(ULK_vector_4d,4)
VSUB(ULK_vector_4d,4)
VZERO(ULK_vector_4d,4)

void ULK_vector_4d_set(ULK_vector_4d out, float x, float y, float z, float w)
{
   out[0] = x;
   out[1] = y;
   out[2] = z;
   out[3] = w;
}
//-------------------------------------
