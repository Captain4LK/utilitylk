/* 
LICENSE OPTION A: 3-clause BSD

   Copyright (C) 2020-2021 Captain4LK (Lukas Holzbeierlein)

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

LICENSE OPTION B: Public Domain - Unlicense

   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <http://unlicense.org/>
*/

/*
Changelog:
   v1.1:
      - added ULK_vector_*d_transform_matrix_*x*()

   v1.1.1:
      - removed all functions allocating memory to make library more usable

   v2.0:
      - rewrite to get rid of expat license terms
   v2.1:
      - cleanup some functions
      - move some functions to macro based implementation
*/

//External includes
#include <math.h>
#include <string.h>
//-------------------------------------

//Internal includes
#include "../include/ULK_matrix.h"
//-------------------------------------

//#defines
#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)<(b)?(a):(b))

#define MADD(mtype,dim) \
   void mtype##_add(mtype out, const mtype a, const mtype b) { for(int i = 0;i<dim*dim;i++) out[i] = a[i]+b[i]; }

#define MCOPY(mtype,dim) \
   void mtype##_copy(mtype dst, const mtype src) { memcpy(dst,src,sizeof(*dst)*dim*dim); }

#define MIDENT(mtype,dim) \
   void mtype##_identity(mtype out) { for(int i = 0;i<dim*dim;i++) out[i] = 0.0f; for(int i = 0;i<dim;i++) out[i*dim+i] = 1.0f; }

#define MSUB(mtype,dim) \
   void mtype##_sub(mtype out, const mtype a, const mtype b) { for(int i = 0;i<dim*dim;i++) out[i] = a[i]-b[i]; }

#define MTRANSPOSE(mtype,dim) \
   void mtype##_transpose(mtype out, const mtype in) { for(int o = 0;o<dim;o++) { for(int i = o+1;i<dim;i++) { float t = in[i*dim+o]; out[i*dim+o] = in[o*dim+i]; out[o*dim+i] = t; } } }

#define MZERO(mtype,dim) \
   void mtype##_zero(mtype out) { for(int i = 0;i<dim*dim;i++) out[i] = 0.0f; }
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
//-------------------------------------

//Function prototypes
//-------------------------------------

//Function implementations

MADD(ULK_matrix_2x2,2)
MCOPY(ULK_matrix_2x2,2)
MIDENT(ULK_matrix_2x2,2)
MSUB(ULK_matrix_2x2,2)
MTRANSPOSE(ULK_matrix_2x2,2)
MZERO(ULK_matrix_2x2,2)

float ULK_matrix_2x2_determinant(const ULK_matrix_2x2 in)
{
   return in[0]*in[3]-in[2]*in[1];
}

void ULK_matrix_2x2_inverse(ULK_matrix_2x2 out, const ULK_matrix_2x2 in)
{
   float in00 = in[0];
   float in11 = in[3];
   float fac = 1.0f/ULK_matrix_2x2_determinant(in);

   out[0] = in11*fac;
   out[1] = -in[1]*fac;
   out[2] = -in[2]*fac;
   out[3] = in00*fac;
}

void ULK_matrix_2x2_mul(ULK_matrix_2x2 out, const ULK_matrix_2x2 a, const ULK_matrix_2x2 b)
{
   ULK_matrix_2x2 ta,tb;
   ULK_matrix_2x2_copy(ta,a);
   ULK_matrix_2x2_copy(tb,b);
   
   out[0] = ta[0]*tb[0]+ta[2]*tb[1];
   out[1] = ta[1]*tb[0]+ta[3]*tb[1];
   out[2] = ta[0]*tb[2]+ta[2]*tb[3];
   out[3] = ta[1]*tb[2]+ta[3]*tb[3];
}

void ULK_matrix_2x2_mul_vector_2d(ULK_vector_2d out, const ULK_matrix_2x2 a, const ULK_vector_2d b)
{
   float x = b[0];

   out[0] = a[0]*b[0]+a[1]*b[1];
   out[1] = a[2]*x+a[3]*b[1];
}

void ULK_matrix_2x2_rotate(ULK_matrix_2x2 out, const ULK_matrix_2x2 in, float angle_rad)
{
   ULK_matrix_2x2 mul;
   ULK_matrix_2x2_set_rotation(mul,angle_rad);
   ULK_matrix_2x2_mul(out,in,mul);
}

void ULK_matrix_2x2_scale(ULK_matrix_2x2 out, const ULK_matrix_2x2 in, float scale)
{
   ULK_matrix_2x2 mul;
   ULK_matrix_2x2_set_scale(mul,scale);
   ULK_matrix_2x2_mul(out,in,mul);
}

void ULK_matrix_2x2_set_rotation(ULK_matrix_2x2 out, float angle_rad)
{
   out[0] = cosf(angle_rad);
   out[1] = sinf(angle_rad);
   out[2] = -out[1];
   out[3] = out[0];
}

void ULK_matrix_2x2_set_scale(ULK_matrix_2x2 out, float scale)
{
   out[0] = scale;
   out[1] = 0.0f;
   out[2] = 0.0f;
   out[3] = scale;
}

MADD(ULK_matrix_3x3,3)
MCOPY(ULK_matrix_3x3,3)
MIDENT(ULK_matrix_3x3,3)
MSUB(ULK_matrix_3x3,3)
MTRANSPOSE(ULK_matrix_3x3,3)
MZERO(ULK_matrix_3x3,3)

float ULK_matrix_3x3_determinant(const ULK_matrix_3x3 in)
{
   return in[0]*(in[4]*in[8]-in[5]*in[7])-in[1]*(in[3]*in[8]-in[5]*in[7])+in[2]*(in[3]*in[7]-in[4]*in[6]);
}

void ULK_matrix_3x3_inverse(ULK_matrix_3x3 out, const ULK_matrix_3x3 in)
{
   ULK_matrix_3x3 tin;
   ULK_matrix_3x3_copy(tin,in);
   float fac = 1.0f/ULK_matrix_3x3_determinant(in);

   out[0] = (tin[4]*tin[8]-tin[7]*tin[5])*fac;
   out[1] = -(tin[3]*tin[8]-tin[6]*tin[5])*fac;
   out[2] = (tin[3]*tin[7]-tin[6]*tin[4])*fac;
   out[3] = -(tin[1]*tin[8]-tin[7]*tin[2])*fac;
   out[4] = (tin[0]*tin[8]-tin[6]*tin[2])*fac;
   out[5] = -(tin[0]*tin[7]-tin[6]*tin[1])*fac;
   out[6] = (tin[1]*tin[5]-tin[4]*tin[2])*fac;
   out[7] = -(tin[0]*tin[5]-tin[3]*tin[2])*fac;
   out[8] = (tin[0]*tin[4]-tin[3]*tin[1])*fac;
} 

void ULK_matrix_3x3_mul(ULK_matrix_3x3 out, const ULK_matrix_3x3 a, const ULK_matrix_3x3 b)
{
   ULK_matrix_3x3 ta,tb;
   ULK_matrix_3x3_copy(ta,a);
   ULK_matrix_3x3_copy(tb,b);

   out[0] = tb[0]*ta[0]+tb[1]*ta[3]+tb[2]*ta[6];
   out[1] = tb[0]*ta[1]+tb[1]*ta[4]+tb[2]*ta[7];
   out[2] = tb[0]*ta[2]+tb[1]*ta[5]+tb[2]*ta[8];
   out[3] = tb[3]*ta[0]+tb[4]*ta[3]+tb[5]*ta[6];
   out[4] = tb[3]*ta[1]+tb[4]*ta[4]+tb[5]*ta[7];
   out[5] = tb[3]*ta[2]+tb[4]*ta[5]+tb[5]*ta[8];
   out[6] = tb[6]*ta[0]+tb[7]*ta[3]+tb[8]*ta[6];
   out[7] = tb[6]*ta[1]+tb[7]*ta[4]+tb[8]*ta[7];
   out[8] = tb[6]*ta[2]+tb[7]*ta[5]+tb[8]*ta[8];
}

void ULK_matrix_3x3_mul_vector_3d(ULK_vector_3d out, const ULK_matrix_3x3 a, const ULK_vector_3d b)
{
   float x = b[0];
   float y = b[1];

   out[0] = a[0]*x+a[1]*y+a[2]*b[2];
   out[1] = a[3]*x+a[4]*y+a[5]*b[2];
   out[2] = a[6]*x+a[7]*y+a[8]*b[2];
}

void ULK_matrix_3x3_rotate_2d(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float angle_rad)
{
   ULK_matrix_3x3 mul;
   ULK_matrix_3x3_set_rotation_2d(mul,angle_rad);
   ULK_matrix_3x3_mul(out,in,mul);
}

void ULK_matrix_3x3_rotate_x(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float angle_rad)
{
   ULK_matrix_3x3 mul;
   ULK_matrix_3x3_set_rotation_x(mul,angle_rad);
   ULK_matrix_3x3_mul(out,in,mul);
}

void ULK_matrix_3x3_rotate_y(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float angle_rad)
{
   ULK_matrix_3x3 mul;
   ULK_matrix_3x3_set_rotation_y(mul,angle_rad);
   ULK_matrix_3x3_mul(out,in,mul);
}

void ULK_matrix_3x3_rotate_z(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float angle_rad)
{
   ULK_matrix_3x3 mul;
   ULK_matrix_3x3_set_rotation_z(mul,angle_rad);
   ULK_matrix_3x3_mul(out,in,mul);
}

void ULK_matrix_3x3_scale(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float scale)
{
   ULK_matrix_3x3 mul;
   ULK_matrix_3x3_set_scale(mul,scale);
   ULK_matrix_3x3_mul(out,in,mul);
}

void ULK_matrix_3x3_scale_2d(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, float scale)
{
   ULK_matrix_3x3 mul;
   ULK_matrix_3x3_set_scale_2d(mul,scale);
   ULK_matrix_3x3_mul(out,in,mul);
}

void ULK_matrix_3x3_set_rotation_2d(ULK_matrix_3x3 out, float angle_rad)
{
   ULK_matrix_3x3_zero(out); 
   out[0] = cosf(angle_rad);
   out[1] = sinf(angle_rad);
   out[3] = -out[1];
   out[4] = out[0];
   out[8] = 1.0f;
}

void ULK_matrix_3x3_set_rotation_x(ULK_matrix_3x3 out, float angle_rad)
{
   out[0] = 1.0f;
   out[1] = 0.0f;
   out[2] = 0.0f;
   out[3] = 0.0f;
   out[4] = cosf(angle_rad);
   out[5] = sinf(angle_rad);
   out[6] = 0.0f;
   out[7] = -out[5];
   out[8] = out[4];
}

void ULK_matrix_3x3_set_rotation_y(ULK_matrix_3x3 out, float angle_rad)
{
   out[0] = cosf(angle_rad);
   out[1] = 0.0f;
   out[3] = 0.0f;
   out[4] = 1.0f;
   out[5] = 0.0f;
   out[6] = sinf(angle_rad);
   out[2] = -out[6];
   out[7] = 0.0f;
   out[8] = out[0];
}

void ULK_matrix_3x3_set_rotation_z(ULK_matrix_3x3 out, float angle_rad)
{
   out[0] = cosf(angle_rad);
   out[1] = sinf(angle_rad);
   out[2] = 0.0f;
   out[3] = -out[1];
   out[4] = out[0];
   out[5] = 0.0f;
   out[6] = 0.0f;
   out[7] = 0.0f;
   out[8] = 1.0f;
}

void ULK_matrix_3x3_set_scale(ULK_matrix_3x3 out, float scale)
{
   ULK_matrix_3x3_zero(out);

   out[0] = scale;
   out[4] = scale;
   out[8] = scale;
}

void ULK_matrix_3x3_set_scale_2d(ULK_matrix_3x3 out, float scale)
{
   ULK_matrix_3x3_zero(out);

   out[0] = scale;
   out[4] = scale;
   out[8] = 1.0f;
}

void ULK_matrix_3x3_set_translation(ULK_matrix_3x3 out, const ULK_vector_2d trans)
{
   ULK_matrix_3x3_identity(out);

   out[6] = trans[0];
   out[7] = trans[1];
}

void ULK_matrix_3x3_translate(ULK_matrix_3x3 out, const ULK_matrix_3x3 in, const ULK_vector_2d trans)
{
   ULK_matrix_3x3 mul;
   ULK_matrix_3x3_set_translation(mul,trans);   
   ULK_matrix_3x3_mul(out,in,mul);
}

MADD(ULK_matrix_4x4,4)
MCOPY(ULK_matrix_4x4,4)
MIDENT(ULK_matrix_4x4,4)
MSUB(ULK_matrix_4x4,4)
MTRANSPOSE(ULK_matrix_4x4,4)
MZERO(ULK_matrix_4x4,4)

void ULK_matrix_4x4_inverse(ULK_matrix_4x4 out, const ULK_matrix_4x4 in)
{
   ULK_matrix_4x4 tin,b;
   ULK_matrix_4x4_copy(tin,in);

   b[0] = tin[0]*tin[5]-tin[1]*tin[4];
   b[1] = tin[0]*tin[6]-tin[2]*tin[4];
   b[2] = tin[0]*tin[7]-tin[3]*tin[4];
   b[3] = tin[1]*tin[6]-tin[2]*tin[5];
   b[4] = tin[1]*tin[7]-tin[3]*tin[5];
   b[5] = tin[2]*tin[7]-tin[3]*tin[6];
   b[6] = tin[8]*tin[13]-tin[9]*tin[12];
   b[7] = tin[8]*tin[14]-tin[10]*tin[12];
   b[8] = tin[8]*tin[15]-tin[11]*tin[12];
   b[9] = tin[9]*tin[14]-tin[10]*tin[13];
   b[10] = tin[9]*tin[15]-tin[11]*tin[13];
   b[11] = tin[10]*tin[15]-tin[11]*tin[14];

   float determinant = b[0]*b[11]-b[1]*b[10]+b[2]*b[9]+b[3]*b[8]-b[4]*b[7]+b[5]*b[6];
   if(determinant!=0.0f)
         determinant = 1.0f/determinant;

   out[0] = (tin[5]*b[11]-tin[6]*b[10]+tin[7]*b[9])*determinant;
   out[1] = (tin[2]*b[10]-tin[1]*b[11]-tin[3]*b[9])*determinant;
   out[2] = (tin[13]*b[5]-tin[14]*b[4]+tin[15]*b[3])*determinant;
   out[3] = (tin[10]*b[4]-tin[9]*b[5]-tin[11]*b[3])*determinant;
   out[4] = (tin[6]*b[8]-tin[4]*b[11]-tin[7]*b[7])*determinant;
   out[5] = (tin[0]*b[11]-tin[2]*b[8]+tin[3]*b[7])*determinant;
   out[6] = (tin[14]*b[2]-tin[12]*b[5]-tin[15]*b[1])*determinant;
   out[7] = (tin[8]*b[5]-tin[10]*b[2]+tin[11]*b[1])*determinant;
   out[8] = (tin[4]*b[10]-tin[5]*b[8]+tin[7]*b[6])*determinant;
   out[9] = (tin[1]*b[8]-tin[0]*b[10]-tin[3]*b[6])*determinant;
   out[10] = (tin[12]*b[4]-tin[13]*b[2]+tin[15]*b[0])*determinant;
   out[11] = (tin[9]*b[2]-tin[8]*b[4]-tin[11]*b[0])*determinant;
   out[12] = (tin[5]*b[7]-tin[4]*b[9]-tin[6]*b[6])*determinant;
   out[13] = (tin[0]*b[9]-tin[1]*b[7]+tin[2]*b[6])*determinant;
   out[14] = (tin[13]*b[1]-tin[12]*b[3]-tin[14]*b[0])*determinant;
   out[15] = (tin[8]*b[3]-tin[9]*b[1]+tin[10]*b[0])*determinant;
}

void ULK_matrix_4x4_look_at(ULK_matrix_4x4 out, ULK_vector_3d from, ULK_vector_3d to, ULK_vector_3d up)
{
   ULK_vector_3d f;
   ULK_vector_3d r;
   ULK_vector_3d s;
   ULK_vector_3d t;
   
   ULK_vector_3d_sub(f,to,from);
   ULK_vector_3d_norm(f,f);
   ULK_vector_3d_cross(s,f,up);
   ULK_vector_3d_norm(s,s);
   ULK_vector_3d_cross(t,s,f);

   out[0] = s[0];
   out[1] = t[0];
   out[2] = -f[0];
   out[3] = 0.0f;

   out[4] = s[1];
   out[5] = t[1];
   out[6] = -f[1];
   out[7] = 0.0f;

   out[8] = s[2];
   out[9] = t[2];
   out[10] = -f[2];
   out[11] = 0.0f;
   
   ULK_vector_3d_set(r,out[0],out[4],out[8]);
   out[12] = ULK_vector_3d_dot(r,from);
   ULK_vector_3d_set(r,out[1],out[5],out[9]);
   out[13] = ULK_vector_3d_dot(r,from);
   ULK_vector_3d_set(r,out[2],out[6],out[10]);
   out[14] = ULK_vector_3d_dot(r,from);
   out[15] = 1.0f;
}

void ULK_matrix_4x4_mul(ULK_matrix_4x4 out, const ULK_matrix_4x4 a, const ULK_matrix_4x4 b)
{
   ULK_matrix_4x4 ta,tb;
   ULK_matrix_4x4_copy(ta,a);
   ULK_matrix_4x4_copy(tb,b);

   out[0] = tb[0]*ta[0]+tb[1]*ta[4]+tb[2]*ta[8]+tb[3]*ta[12];
   out[1] = tb[0]*ta[1]+tb[1]*ta[5]+tb[2]*ta[9]+tb[3]*ta[13];
   out[2] = tb[0]*ta[2]+tb[1]*ta[6]+tb[2]*ta[10]+tb[3]*ta[14];
   out[3] = tb[0]*ta[3]+tb[1]*ta[7]+tb[2]*ta[11]+tb[3]*ta[15];
   out[4] = tb[4]*ta[0]+tb[5]*ta[4]+tb[6]*ta[8]+tb[7]*ta[12];
   out[5] = tb[4]*ta[1]+tb[5]*ta[5]+tb[6]*ta[9]+tb[7]*ta[13];
   out[6] = tb[4]*ta[2]+tb[5]*ta[6]+tb[6]*ta[10]+tb[7]*ta[14];
   out[7] = tb[4]*ta[3]+tb[5]*ta[7]+tb[6]*ta[11]+tb[7]*ta[15];
   out[8] = tb[8]*ta[0]+tb[9]*ta[4]+tb[10]*ta[8]+tb[11]*ta[12];
   out[9] = tb[8]*ta[1]+tb[9]*ta[5]+tb[10]*ta[9]+tb[11]*ta[13];
   out[10] = tb[8]*ta[2]+tb[9]*ta[6]+tb[10]*ta[10]+tb[11]*ta[14];
   out[11] = tb[8]*ta[3]+tb[9]*ta[7]+tb[10]*ta[11]+tb[11]*ta[15];
   out[12] = tb[12]*ta[0]+tb[13]*ta[4]+tb[14]*ta[8]+tb[15]*ta[12];
   out[13] = tb[12]*ta[1]+tb[13]*ta[5]+tb[14]*ta[9]+tb[15]*ta[13];
   out[14] = tb[12]*ta[2]+tb[13]*ta[6]+tb[14]*ta[10]+tb[15]*ta[14];
   out[15] = tb[12]*ta[3]+tb[13]*ta[7]+tb[14]*ta[11]+tb[15]*ta[15];
}

void ULK_matrix_4x4_mul_vector_3d(ULK_vector_3d out, const ULK_matrix_4x4 a, const ULK_vector_3d b)
{
   ULK_vector_4d tb;
   ULK_vector_4d_set(tb,b[0],b[1],b[2],a[3]*b[0]+a[7]*b[1]+a[11]*b[2]+a[15]);

   if(tb[3]==0.0f)
      tb[3] = 1.0f;

   out[0] = (a[0]*tb[0]+a[4]*tb[1]+a[8]*tb[2]+a[12])/tb[3];
   out[1] = (a[1]*tb[0]+a[5]*tb[1]+a[9]*tb[2]+a[13])/tb[3];
   out[2] = (a[2]*tb[0]+a[6]*tb[1]+a[10]*tb[2]+a[14])/tb[3];
}

void ULK_matrix_4x4_mul_vector_4d(ULK_vector_4d out, const ULK_matrix_4x4 a, const ULK_vector_4d b)
{
   ULK_vector_4d tb;
   ULK_vector_4d_copy(tb,b);

   out[0] = a[0]*tb[0]+a[4]*tb[1]+a[8]*tb[2]+a[12]*tb[3];
   out[1] = a[1]*tb[0]+a[5]*tb[1]+a[9]*tb[2]+a[13]*tb[3];
   out[2] = a[2]*tb[0]+a[6]*tb[1]+a[10]*tb[2]+a[14]*tb[3];
   out[3] = a[3]*tb[0]+a[7]*tb[1]+a[11]*tb[2]+a[15]*tb[3];
}

void ULK_matrix_4x4_ortho(ULK_matrix_4x4 out, float left, float right, float bottom, float top, float near, float far)
{
   ULK_matrix_4x4_identity(out);

   out[0] = 2.0f/(right-left);
   out[5] = 2.0f/(top-bottom);
   out[10] = -1.0f;
   out[12] = -(right+left)/(right-left);
   out[13] = -(top+bottom)/(top-bottom);
}

void ULK_matrix_4x4_perspective(ULK_matrix_4x4 out, float angle, float aspect, float near, float far)
{
   float a = 1.0f/tanf(angle*0.5f);
   
   out[0] = a/aspect;
   out[1] = 0.0f;
   out[2] = 0.0f;
   out[3] = 0.0f;
   
   out[4] = 0.0f;
   out[5] = a;
   out[6] = 0.0f;
   out[7] = 0.0f;

   out[8] = 0.0f;
   out[9] = 0.0f;
   out[10] = (far+near)/(near-far);
   out[11] = -1.0f;

   out[12] = 0.0f;
   out[13] = 0.0f;
   out[14] = ((2.0f*far*near)/(near-far));
   out[15] = 0.0f;
}

void ULK_matrix_4x4_rotate(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, const ULK_vector_3d rot_axis, float angle_rad)
{
   ULK_matrix_4x4 mul;
   ULK_matrix_4x4_set_rotation(mul,rot_axis,angle_rad);
   ULK_matrix_4x4_mul(out,in,mul);
}

void ULK_matrix_4x4_rotate_x(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, float angle_rad)
{
   ULK_matrix_4x4 mul;
   ULK_matrix_4x4_set_rotation_x(mul,angle_rad);
   ULK_matrix_4x4_mul(out,in,mul);
}

void ULK_matrix_4x4_rotate_y(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, float angle_rad)
{
   ULK_matrix_4x4 mul;
   ULK_matrix_4x4_set_rotation_y(mul,angle_rad);
   ULK_matrix_4x4_mul(out,in,mul);

}

void ULK_matrix_4x4_rotate_z(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, float angle_rad)
{
   ULK_matrix_4x4 mul;
   ULK_matrix_4x4_set_rotation_z(mul,angle_rad);
   ULK_matrix_4x4_mul(out,in,mul);
}

void ULK_matrix_4x4_scale(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, float scale)
{
   ULK_matrix_4x4 mul;
   ULK_matrix_4x4_set_scale(mul,scale);
   ULK_matrix_4x4_mul(out,in,mul);
}

void ULK_matrix_4x4_scale_3d(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, ULK_vector_3d scale)
{
   ULK_matrix_4x4 mul;
   ULK_matrix_4x4_set_scale_3d(mul,scale);
   ULK_matrix_4x4_mul(out,in,mul);
}

void ULK_matrix_4x4_set_scale(ULK_matrix_4x4 out, float scale)
{
   ULK_matrix_4x4_zero(out);

   out[0] = scale;
   out[5] = scale;
   out[10] = scale;
   out[15] = 1.0f;
}

void ULK_matrix_4x4_set_scale_3d(ULK_matrix_4x4 out, ULK_vector_3d scale)
{
   ULK_matrix_4x4_zero(out);

   out[0] = scale[0];
   out[5] = scale[1];
   out[10] = scale[2];
   out[15] = 1.0f;
}

void ULK_matrix_4x4_set_rotation(ULK_matrix_4x4 out, const ULK_vector_3d rot_axis, float angle_rad)
{
   float c,s;
   ULK_vector_3d axis,t;

   c = cosf(angle_rad);
   s = sinf(angle_rad);

   ULK_vector_3d_norm(axis,rot_axis);
   ULK_vector_3d_mul(t,axis,1.0f-c);

   ULK_matrix_4x4_identity(out);

   out[0] = c*t[0]*axis[0];
   out[1] = t[0]*axis[1]+s*axis[2];
   out[2] = t[0]*axis[2]-s*axis[1];
   out[3] = 0.0f;

   out[4] = t[1]*axis[0]-s*axis[2];
   out[5] = c+t[1]*axis[1];
   out[6] = t[1]*axis[2]+s*axis[0];
   out[7] = 0.0f;

   out[8] = t[2]*axis[0]+s*axis[1];
   out[9] = t[2]*axis[1]-s*axis[0];
   out[10] = c+t[2]*axis[2];
   out[11] = 0.0f;
}

void ULK_matrix_4x4_set_rotation_x(ULK_matrix_4x4 out, float angle_rad)
{
   ULK_matrix_4x4_zero(out);

   out[0] = 1.0f;
   out[5] = cosf(angle_rad);
   out[6] = sinf(angle_rad);
   out[9] = -out[6];
   out[10] = out[5];
   out[15] = 1.0f;
}

void ULK_matrix_4x4_set_rotation_y(ULK_matrix_4x4 out, float angle_rad)
{
   ULK_matrix_4x4_zero(out);

   out[0] = cosf(angle_rad);
   out[2] = -out[8];
   out[5] = 1.0f;
   out[8] = sinf(angle_rad);
   out[10] = out[0];
   out[15] = 1.0f;
}

void ULK_matrix_4x4_set_rotation_z(ULK_matrix_4x4 out, float angle_rad)
{
   ULK_matrix_4x4_zero(out);

   out[0] = cosf(angle_rad);
   out[1] = sinf(angle_rad);
   out[4] = -out[1];
   out[5] = out[0];
   out[10] = 1.0f;
   out[15] = 1.0f;
}

void ULK_matrix_4x4_set_translation(ULK_matrix_4x4 out, const ULK_vector_3d trans)
{
   ULK_matrix_4x4_identity(out);
   out[12] = trans[0];
   out[13] = trans[1];
   out[14] = trans[2];
   out[15] = 1.0f;
}

void ULK_matrix_4x4_translate(ULK_matrix_4x4 out, const ULK_matrix_4x4 in, const ULK_vector_3d trans)
{
   ULK_matrix_4x4 mul;
   ULK_matrix_4x4_set_translation(mul,trans);
   ULK_matrix_4x4_mul(out,in,mul);
}
//-------------------------------------
