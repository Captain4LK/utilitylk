/* 
LICENSE OPTION A: 3-clause BSD

   Copyright (C) 2020-2021 Captain4LK (Lukas Holzbeierlein)

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

LICENSE OPTION B: Public Domain - Unlicense
   
   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <http://unlicense.org/>
*/

/*
Changelog:
   v1.1:
      - added ULK_vertex_winding()
*/

//External includes
#include <stdio.h>
#include <string.h>
#include <math.h>
//-------------------------------------

//Internal includes
#include "../include/ULK_3d.h"
//-------------------------------------

//#defines
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
#define EPSILON 0.01f
#define ULK_3D_MAX_CLIP 32
//-------------------------------------

//Function prototypes
//-------------------------------------

//Function implementations

//Makes the plane face the position.
void ULK_plane_face(ULK_plane *plane, float x, float y, float z)
{
   float d = plane->a*x+plane->b*y+plane->c*z-plane->d;

   if(d>0)
   {
      plane->a*=-1;
      plane->b*=-1;
      plane->c*=-1;
      plane->d*=-1;
   }
}

//Makes the plane face the position.
//Vector variant.
void ULK_plane_face_vector(ULK_plane *plane, const ULK_vector_3d pos)
{
   float d = plane->a*pos[0]+plane->b*pos[1]+plane->c*pos[2]-plane->d;

   if(d>0)
   {
      plane->a*=-1;
      plane->b*=-1;
      plane->c*=-1;
      plane->d*=-1;
   }
}

//Makes the plane not face the position.
void ULK_plane_unface(ULK_plane *plane, float x, float y, float z)
{
   ULK_plane_face(plane,x,y,z);
   plane->a*=-1;
   plane->b*=-1;
   plane->c*=-1;
   plane->d*=-1;
}

//Returns wether the plane is facing the specified position.
int ULK_plane_facing(const ULK_plane *plane, float x, float y, float z)
{
   float r = plane->a*x+plane->b*y+plane->c*z-plane->d;
   return r>-EPSILON;
}

//Returns wether the plane is facing the specified position.
//Vector variant.
int ULK_plane_facing_vector(const ULK_plane *plane, const ULK_vector_3d pos)
{
   float r = plane->a*pos[0]+plane->b*pos[1]+plane->c*pos[2]-plane->d;
   return r>-EPSILON;
}

//Creates a plane from 3 positions.
void ULK_plane_from_pos(ULK_plane *plane, const ULK_vector_3d pos0, const ULK_vector_3d pos1, const ULK_vector_3d pos2)
{
   float x1,y1,z1,x2,y2,z2;

   x1 = pos1[0]-pos0[0];
   y1 = pos1[1]-pos0[1];
   z1 = pos1[2]-pos0[2];
   x2 = pos2[0]-pos0[0];
   y2 = pos2[1]-pos0[1];
   z2 = pos2[2]-pos0[2];

   plane->a = y1*z2-y2*z1;
   plane->b = z1*x2-z2*x1;
   plane->c = x1*y2-x2*y1;

   float len = sqrtf(plane->a*plane->a+plane->b*plane->b+plane->c*plane->c);

   plane->a/=len;
   plane->b/=len;
   plane->c/=len;
   plane->d = plane->a*pos1[0]+plane->b*pos1[1]+plane->c*pos1[2];
}

//Creates a plane from a polygon.
void ULK_plane_from_polygon(ULK_plane *plane, ULK_polygon *poly)
{
   float x1,y1,z1,x2,y2,z2;
   ULK_vertex *v1 = poly->vertices;
   ULK_vertex *v2 = v1->next;
   ULK_vertex *v3 = v2->next;

   x1 = v2->pos[0]-v1->pos[0]; 
   y1 = v2->pos[1]-v1->pos[1];
   z1 = v2->pos[2]-v1->pos[2];

   while(v3)
   {
      x2 = v3->pos[0]-v1->pos[0];
      y2 = v3->pos[1]-v1->pos[1];
      z2 = v3->pos[2]-v1->pos[2];
      plane->a = y1*z2-y2*z1;
      plane->b = z1*x2-z2*x1;
      plane->c = x1*y2-x2*y1;

      if(plane->a!=0.0f||plane->b!=0.0f||plane->c!=0.0f)
         break;

      v3 = v3->next;
   }

   float len = sqrtf(plane->a*plane->a+plane->b*plane->b+plane->c*plane->c);
   plane->a/=len;
   plane->b/=len;
   plane->c/=len;
   plane->d = plane->a*v1->pos[0]+plane->b*v1->pos[1]+plane->c*v1->pos[2];
}

//Makes the plane not face the position.
//Vector variant.
void ULK_plane_unface_vector(ULK_plane *plane, const ULK_vector_3d pos)
{
   ULK_plane_face_vector(plane,pos);
   plane->a*=-1;
   plane->b*=-1;
   plane->c*=-1;
   plane->d*=-1;
}

//Normalizes the normal vector of a plane.
void ULK_plane_normalize(ULK_plane *out, const ULK_plane *in)
{
   float length = sqrtf(in->a*in->a+in->b*in->b+in->c*in->c);
   if(length==0.0f)
      length = 1.0f;
   out->a = in->a/length;
   out->b = in->b/length;
   out->c = in->c/length;
   out->d = in->d/length;
}

//Sets all plane components to 0.
void ULK_plane_reset(ULK_plane *plane)
{
   plane->a = 0.0f;
   plane->b = 0.0f;
   plane->c = 0.0f;
   plane->d = 0.0f;
}

//Swaps the sign of the plane components.
void ULK_plane_sigswap(ULK_plane *plane)
{
   plane->a*=-1;
   plane->b*=-1;
   plane->c*=-1;
   plane->d*=-1;
}

//Sets the values of the plane.
void ULK_plane_set(ULK_plane *plane, float a, float b, float c, float d)
{
   plane->a = a;
   plane->b = b;
   plane->c = c;
   plane->d = d;
}

//Tranforms a plane by the given matrix.
void ULK_plane_transform(ULK_plane *out, const ULK_plane *in, const ULK_matrix_4x4 m)
{
   ULK_matrix_4x4 mat;
   ULK_vector_4d vec;
   ULK_matrix_4x4_copy(mat,m);
   ULK_vector_4d_set(vec,in->a,in->b,in->c,in->d);

   ULK_matrix_4x4_inverse(mat,mat);
   ULK_matrix_4x4_transpose(mat,mat);
   ULK_matrix_4x4_mul_vector_4d(vec,mat,vec);

   out->a = vec[0];
   out->b = vec[1];
   out->c = vec[2];
   out->d = vec[3];
}

//Clips a polygon agains a plain.
//Pass 1 for processed to clip the processed positions
//instead of the unprocessed ones.
//DO NOT modify the resulting polygon, first copy the values 
//to a newly allocated vertex linked list before modifiyng!
static ULK_vertex temp_vertices[1000];
static int temp_vert_num = 0;
ULK_vertex *ULK_vertex_clip(ULK_vertex *poly, const ULK_plane *plane, int processed)
{
   if(poly==NULL||poly->next==NULL)
      return NULL;

   ULK_vector_3d plane_normal;
   ULK_vertex *v = poly;
   ULK_vertex *vt[ULK_3D_MAX_CLIP];
   ULK_vertex *out;
   ULK_vertex *out_next;
   int curv = 0;
   float cur_dot;
   float next_dot;
   float scale;
   int cur_in;
   int next_in;

   ULK_vector_3d_set(plane_normal,plane->a,plane->b,plane->c);
   if(processed)
      cur_dot = ULK_vector_3d_dot(poly->pos_pr,plane_normal);
   else
      cur_dot = ULK_vector_3d_dot(poly->pos,plane_normal);

   cur_in = (cur_dot>=plane->d);
   
   while(v)
   {
      ULK_vertex *next_vert = v->next?v->next:poly;

      if(cur_in)
      {
         ULK_vertex *t = &temp_vertices[temp_vert_num++];
         t->next = NULL;
         ULK_vector_3d_copy(t->pos,v->pos);
         ULK_vector_3d_copy(t->pos_pr,v->pos_pr);
         t->u = v->u;
         t->v = v->v;
         vt[curv++] = t;
      }

      if(processed)
         next_dot = ULK_vector_3d_dot(next_vert->pos_pr,plane_normal);
      else
         next_dot = ULK_vector_3d_dot(next_vert->pos,plane_normal);

      next_in = (next_dot>=plane->d);

      if(cur_in!=next_in)
      {
         scale = (plane->d-cur_dot)/(next_dot-cur_dot);
         ULK_vertex *t = &temp_vertices[temp_vert_num++];
         t->next = NULL;
         ULK_vector_3d_lerp(t->pos_pr,v->pos_pr,next_vert->pos_pr,scale);
         ULK_vector_3d_lerp(t->pos,v->pos,next_vert->pos,scale);
         t->u = v->u+(next_vert->u-v->u)*scale;
         t->v = v->v+(next_vert->v-v->v)*scale;
         vt[curv++] = t;
      }

      cur_dot = next_dot;
      cur_in = next_in;
      v = v->next;
   }

   if(curv<3)
      return NULL;

   out = vt[0];
   out_next = out;
   
   for(int i = 1;i<curv;i++)
   {
      out_next->next = vt[i];
      out_next = out_next->next;
   }

   return out;
}

void ULK_vertex_copy(ULK_vertex *dst, const ULK_vertex *src)
{
   memcpy(dst,src,sizeof(ULK_vertex));
}

//Resets the temporary vertices. 
//Call this when you don't need the clipped vertices anymore.
void ULK_vertex_reset_temp()
{
   temp_vert_num = 0;
}

//Counts the vertices in a vertex linked list.
int ULK_vertex_count(ULK_vertex *vertex)
{
   ULK_vertex *v = vertex;
   int count = 0;

   while(v)
   {
      count++;
      v = v->next;
   }
   return count;
}

//Returns the winding direction of the polygon, 
//only works if z is the same for all vertices.
//Returns 1 if the polygon is winded clockwise.
int ULK_vertex_winding(ULK_vertex *vertex, int processed)
{
   float area = 0.0f;

   ULK_vertex *v = vertex; 
   while(v)
   {
      ULK_vertex *next = v->next?v->next:vertex;

      if(processed)
         area+=(v->pos_pr[0]*next->pos_pr[1]-next->pos_pr[0]*v->pos_pr[1]);
      else
         area+=(v->pos[0]*next->pos[1]-next->pos[0]*v->pos[1]);

      v = v->next;
   }

   return area>0.0f;
}

//Adds a plane to the frustum.
void ULK_frustum_add_plane(ULK_frustum *frustum, ULK_plane *plane)
{
   frustum->planes[frustum->num_planes++] = plane;
}

//Clips a polygon against all planes in a frustum.
ULK_vertex *ULK_frustum_clip(const ULK_frustum *frustum, ULK_vertex *verts, int processed)
{
   ULK_vertex *out = verts;

   for(int i = 0;i<frustum->num_planes;i++)
   {
      ULK_vertex *next = out;
      out = ULK_vertex_clip(next,frustum->planes[i],processed);
   }
   
   return out;
}

//Creates a frustum from a projection matrix.
void ULK_frustum_create(ULK_frustum *out, const ULK_matrix_4x4 projection)
{
   for(int i = 0;i<6;i++)
   {
      if(out->planes[i]==NULL)
      {
         printf("No memory allocated for plane %d\n",i);
         return;
      }
   }

   out->num_planes = 5;

   //Right (what the fuck?)
   out->planes[0]->a = -(projection[3]+projection[0]);
   out->planes[0]->b = -(projection[7]+projection[4]);
   out->planes[0]->c = -(projection[11]+projection[8]);
   out->planes[0]->d = projection[15]+projection[12];
   ULK_plane_normalize(out->planes[0],out->planes[0]);

   //Left
   out->planes[1]->a = -(projection[3]-projection[0]);
   out->planes[1]->b = -(projection[7]-projection[4]);
   out->planes[1]->c = -(projection[11]-projection[8]);
   out->planes[1]->d = projection[15]-projection[12];
   ULK_plane_normalize(out->planes[1],out->planes[1]);

   //Bottom
   out->planes[2]->a = -(projection[3]+projection[1]);
   out->planes[2]->b = -(projection[7]+projection[5]);
   out->planes[2]->c = -(projection[11]+projection[9]);
   out->planes[2]->d = projection[15]+projection[13];
   ULK_plane_normalize(out->planes[2],out->planes[2]);

   //Top
   out->planes[3]->a = -(projection[3]-projection[1]);
   out->planes[3]->b = -(projection[7]-projection[5]);
   out->planes[3]->c = -(projection[11]-projection[9]);
   out->planes[3]->d = projection[15]-projection[13];
   ULK_plane_normalize(out->planes[3],out->planes[3]);

   //Near 
   out->planes[4]->a = -(projection[3]+projection[2]);
   out->planes[4]->b = -(projection[7]+projection[6]);
   out->planes[4]->c = -(projection[11]+projection[10]);
   out->planes[4]->d = projection[15]+projection[14];
   ULK_plane_normalize(out->planes[4],out->planes[4]);

   //Far
   out->planes[5]->a = -(projection[3]-projection[2]);
   out->planes[5]->b = -(projection[7]-projection[6]);
   out->planes[5]->c = -(projection[11]-projection[10]);
   out->planes[5]->d = projection[15]-projection[14];
   ULK_plane_normalize(out->planes[5],out->planes[5]);
}

//Returns the specified plane.
ULK_plane *ULK_frustum_get_plane(const ULK_frustum *frustum, int plane)
{
   return frustum->planes[plane];
}

//Returns how many planes are currently used.
int ULK_frustum_get_plane_num(const ULK_frustum *frustum)
{
   return frustum->num_planes;
}

//Sets wich plane to set next by setting the number of planes that are currently stored.
//Pass 0 to "clear" the frustrum.
void ULK_frustum_set_plane(ULK_frustum *frustum, int planes)
{
   frustum->num_planes = planes;
}

//Calculates the distance between a plane and a vector.
float ULK_vector_3d_distance_plane(const ULK_vector_3d a, const ULK_plane *b)
{
   return b->a*a[0]+b->b*a[1]+b->c*a[2]-b->d;
}
//-------------------------------------
