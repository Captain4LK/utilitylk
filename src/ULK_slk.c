/* 
LICENSE OPTION A: 3-clause BSD

   Copyright (C) 2020-2021 Captain4LK (Lukas Holzbeierlein)

   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

   3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

LICENSE OPTION B: Public Domain - Unlicense

   This is free and unencumbered software released into the public domain.

   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.

   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.

   For more information, please refer to <http://unlicense.org/>
*/

/*
Changelog:
   v1.0:
      - added error information
      - code cleanup
*/

//External includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
//-------------------------------------

//Internal includes
#include "../include/ULK_slk.h"
//-------------------------------------

//#defines

//Macro for shortened error handling
#define CHECK(in,text_fail,action) \
   if(!in) { error = text_fail; action; }

#define MIN(a,b) \
   ((a)<(b)?(a):(b))
//-------------------------------------

//Typedefs
typedef struct
{
   char ident[8];
   int32_t width;
   int32_t height;
   int32_t comp;
}Header;
//-------------------------------------

//Variables
static const char *error = NULL;
//-------------------------------------

//Function prototypes
//-------------------------------------

//Function implementations

ULK_slk_image *ULK_slk_image_load(const char *path)
{
   FILE *f = fopen(path,"rb");
   CHECK(f,"UKL_slk_image_load: failed to open file",return NULL)

   ULK_slk_image *img = ULK_slk_image_load_file(f); 
   fclose(f);

   return img;
}

ULK_slk_image *ULK_slk_image_load_file(FILE *f)
{
   CHECK(f,"ULK_slk_image_load_file: file pointer is NULL",return NULL)

   char *buffer = NULL;
   int size = 0;
   fseek(f,0,SEEK_END);
   size = ftell(f);
   fseek(f,0,SEEK_SET);

   buffer = malloc(size+1);
   CHECK(buffer,"ULK_slk_image_load_file: malloc failed (out of memory)",return NULL)

   fread(buffer,size,1,f);
   buffer[size] = 0;
   ULK_slk_image *img = ULK_slk_image_load_mem_buffer(buffer,size);
   free(buffer);

   return img;
}

ULK_slk_image *ULK_slk_image_load_mem_buffer(const void *data, int length)
{
   const Header *h = (const Header *)data;
   CHECK((strncmp("SLKIMAGE",h->ident,8)==0),"ULK_slk_image_load_mem_buffer: memory does not seem to be slk file",return NULL)

   ULK_slk_image *s = malloc(sizeof(*s));
   CHECK(s,"ULK_slk_image_load_mem_buffer: malloc failed (out of memory)",return NULL)
   s->width = h->width;
   s->height = h->height;
   s->data = malloc(sizeof(*s->data)*s->width*s->height);
   CHECK(s->data,"ULK_slk_image_load_mem_buffer: malloc failed (out of memory)",free(s);return NULL)
   memset(s->data,1,s->width*s->height);
   int data_ptr = sizeof(*h)+7;
   if(data_ptr>=length)
      return s;

   //Index rle encoding 
   if(h->comp&1)
   {
      int end = s->width*s->height;
      int i = 0;
      while(i<end)
      {
         uint8_t lengthl = *((uint8_t *)data+data_ptr);
         data_ptr++;
         if(data_ptr>=length)
            break;
         uint8_t byte = *((uint8_t *)data+data_ptr);
         data_ptr++;
         if(data_ptr>=length)
            break;

         lengthl = MIN(lengthl,(end-i));
         for(int o = 0;o<lengthl;o++)
         {
            s->data[i] = byte;
            i++;
         }
      }
   }
   //No rle encoding
   else
   {
      int end = s->width*s->height;
      for(int i = 0;i<end;i++)
      {
         s->data[i] = *((char *)data+data_ptr);
         data_ptr++;
         if(data_ptr>=length)
            break;
      }
   }

   CHECK((strncmp("INDICES_END",(char *)data+data_ptr,11)==0),"ULK_slk_image_load_mem_buffer: error in image file",free(s->data);free(s);return NULL)

   return s;
}

void ULK_slk_image_free(ULK_slk_image *i)
{
   free(i->data);
   free(i);
}

void ULK_slk_image_write(ULK_slk_image *i, FILE *f, int32_t comp)
{
   Header h;
   strcpy(h.ident,"SLKIMAG");
   h.ident[7] = 'E';
   h.width = i->width;
   h.height = i->height;
   h.comp = comp;
   fwrite(&h,sizeof(h),1,f);
   fprintf(f,"INDICES");

   if(comp&1) //RLE encode indices
   {
      uint8_t length = 1;
      int end = i->width*i->height;
      uint8_t current = i->data[0];
      for(int o = 1;o<end;o++)
      {
         if(i->data[o]==current)
         {
            length++;
            if(length==255)
            {
               fputc(length,f);
               fputc(current,f);
               length = 0;
            }
         }
         else
         {
            fputc(length,f);
            fputc(current,f);
            length = 1;
            current = i->data[o];
         }
      }
      fputc(length,f);
      fputc(current,f);
   }
   else
   {
      for(int o = 0;o<i->width*i->height;o++)
         fputc(i->data[o],f);
   }
   fprintf(f,"INDICES_END");
}

const char *ULK_slk_get_error()
{
   return error;
}
//-------------------------------------
