# UtilityLK

This is a collection of some random C libraries I have created over time.

## License

All libraries are dual licensed.
You can either use them under the terms of the 3-clause BSD license, or treat them as public domain code. See LICENSE or the source files for details.

## What's in here

This is a collection of some random C libraries I have created over time.

|library|version|category|description|
|---|---|---|---|
|[ULK_vector](include/ULK_vector.h)|2.1|math|simple vector math library with support for 2,3 and 4 dimensional vectors|
|[ULK_matrix](include/ULK_matrix.h)|2.1|math|simple matrix math library with support for 2x2,3x3 and 4x4 matrix, needs ULK_vector to work|
|[ULK_3d](include/ULK_3d.h)|1.0.1|math|3d graphics math basic, planes, clipping, frustums, needs ULK_vector and ULK_matrix|
|[ULK_macro](include/ULK_macro.h)|0.1|macro|some usefull macros|
|[ULK_json](include/ULK_json.h)|1.3|data|extension of tinyjson5 by r-lyeh|
|[ULK_fixed](include/ULK_fixed.h)|0.8|math|fixed point math|
|[ULK_slk](include/ULK_slk.h)|1.0|data|image format used by SoftLK-lib|

## How to use

Just grab the header file and the corresponding c source file and add them to your project. Read the information in the header file for more information.
